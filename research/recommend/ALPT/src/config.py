# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import argparse

def argparse_init():
    parser = argparse.ArgumentParser(description='DCN')
    parser.add_argument("--emb_type", type=str, default="fp32", help="fp32/lpt/alpt")
    parser.add_argument("--device_id", type=int, default=2, help="device id")
    parser.add_argument("--epochs", type=int, default=1, help="Total train epochs")
    parser.add_argument("--batch_size", type=int, default=10000, help="Training batch size.")
    parser.add_argument("--emb_dim", type=int, default=16, help="The dense embedding dimension of sparse feature.")
    parser.add_argument("--deep_layer_dim", type=int, nargs='+', default=[1024, 1024], help="dimension.")
    parser.add_argument("--cross_layer_num", type=int, default=6, help="Cross layer num")
    parser.add_argument("--deep_layer_act", type=str, default='relu', help="activation function.")
    parser.add_argument("--keep_prob", type=float, default=1.0, help="The keep rate in dropout layer.")
    parser.add_argument("--dropout_flag", type=int, default=0, help="Enable dropout")
    parser.add_argument("--output_path", type=str, default="./output/")
    parser.add_argument("--ckpt_path", type=str, default="./checkpoints/", help="The location of the checkpoint file.")
    parser.add_argument("--eval_file_name", type=str, default="eval.log", help="Eval output file.")
    parser.add_argument("--loss_file_name", type=str, default="loss.log", help="Loss output file.")
    return parser

class DeepCrossConfig():
    def __init__(self):
        self.emb_type = "fp32"

        self.device_id = 0
        self.epochs = 10
        self.batch_size = 10000
        self.field_size = 39
        self.vocab_size = 200000
        self.emb_dim = 16
        self.deep_layer_dim = [1024, 1024]
        self.cross_layer_num = 6
        self.deep_layer_act = 'relu'
        self.weight_bias_init = ['normal', 'normal']
        self.init_args = [-0.01, 0.01]
        self.dropout_flag = False
        self.keep_prob = 1.0

        self.output_path = "./output"
        self.eval_file_name = "eval.log"
        self.loss_file_name = "loss.log"
        self.ckpt_path = "./checkpoints/"

    def argparse_init(self):
        parser = argparse_init()
        args, _ = parser.parse_known_args()
        self.emb_type = args.emb_type

        self.device_id = args.device_id
        self.epochs = args.epochs
        self.batch_size = args.batch_size
        self.emb_dim = args.emb_dim
        self.deep_layer_dim = args.deep_layer_dim
        self.cross_layer_num = args.cross_layer_num
        self.deep_layer_act = args.deep_layer_act
        self.weight_bias_init = ['normal', 'normal']
        self.init_args = [-0.01, 0.01]
        self.dropout_flag = bool(args.dropout_flag)
        self.keep_prob = args.keep_prob

        self.output_path = args.output_path
        self.eval_file_name = args.eval_file_name
        self.loss_file_name = args.loss_file_name
        self.ckpt_path = args.ckpt_path
