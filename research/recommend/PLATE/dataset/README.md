# Douban dataset

You can download the Douban dataset here: https://drive.google.com/file/d/1sqy89WuOgXv7L4HRRQKehuVPHvx4oyaf/view?usp=sharing,
which has already been divided into train, valid, and test set.
