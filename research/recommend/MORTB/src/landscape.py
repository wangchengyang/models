# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
code for predicting the auction landscape
"""
from scipy.stats import lognorm
import numpy as np

SCORE = ['p1_ecpm', 'p2_ecpm', 'p3_ecpm', 'p4_ecpm', 'p5_ecpm', 'p6_ecpm', 'p7_ecpm', 'p8_ecpm', 'p9_ecpm', 'p10_ecpm']


class Landscape:
    def __init__(self, data, len_list=10, u_k=None, gamma=1):
        self.len_list = len_list
        if u_k is None or len_list <= 10:
            self.u_k = (np.array([0.68, 0.61, 0.48, 0.34, 0.28, 0.20, 0.11, 0.10, 0.08, 0.06][:self.len_list])
                        .reshape(-1, 1))**gamma
        else:
            self.u_k = (np.array(u_k).reshape(-1, 1))**gamma
        assert len(self.u_k) == self.len_list
        np_s = []
        for p in SCORE:
            np_s.append(np.array(data[p].tolist()))
        self.mus, self.sigmas, self.scales = [], [], []
        for p in range(self.len_list):
            self.mus.append(np.log(np_s[p]).mean())
            self.sigmas.append(np.log(np_s[p]).std())
            self.scales.append(np.exp(self.mus[p]))

        self.Phi_list = self.getPhi_FP(np.linspace(0.00001, 0.03, 10000))
        (self.f_list, self.F_list, self.f2_list, self.Phi_list) = self._get3dist(np.linspace(0.00001, 0.03, 10000))

    def _getPdfDrv(self, k, score):
        return np.exp(-(np.log(score) - self.mus[k]) ** 2 / (2 * self.sigmas[k] ** 2)) \
                / (-score * score * self.sigmas[k] * np.sqrt(2 * np.pi)) *\
               (1 + (np.log(score) - self.mus[k]) / self.sigmas[k] ** 2)

    def getWinRate(self, k, score):
        if k == 0:
            return lognorm.cdf(score, self.sigmas[k], scale=self.scales[k])
        return lognorm.cdf(score, self.sigmas[k], scale=self.scales[k]) \
            - lognorm.cdf(score, self.sigmas[k - 1], scale=self.scales[k - 1])

    def getWinRateDrv(self, k, score):
        if k == 0:
            return lognorm.pdf(score, self.sigmas[k], scale=self.scales[k])
        return (lognorm.pdf(score, self.sigmas[k], scale=self.scales[k])
                      - lognorm.pdf(score, self.sigmas[k - 1], scale=self.scales[k - 1]))

    def getPhi_FP(self, score):
        nume = 0
        deno = 0
        for k in range(self.len_list):
            nume += self.u_k[k] * self.getWinRate(k, score)
            deno += self.u_k[k] * self.getWinRateDrv(k, score)
        return nume / deno

    def _get3dist(self, score):
        F, f, f2 = 0, 0, 0
        for k in range(1, self.len_list):
            F += (self.u_k[k-1] - self.u_k[k]) * lognorm.cdf(score, self.sigmas[k-1], scale=self.scales[k-1])
            f += (self.u_k[k-1] - self.u_k[k]) * lognorm.pdf(score, self.sigmas[k-1], scale=self.scales[k-1])
            f2 += (self.u_k[k-1] - self.u_k[k]) * self._getPdfDrv(k-1, score)
        F += self.u_k[-1] * lognorm.cdf(score, self.sigmas[-1], scale=self.scales[-1])
        f += self.u_k[-1] * lognorm.pdf(score, self.sigmas[-1], scale=self.scales[-1])
        f2 += self.u_k[-1] * self._getPdfDrv(-1, score)
        return (f, F, f2, F / f)

    def getNewtonBid(self, val, r, lda):
        max_it = 0
        b = val / 2
        b0 = b
        while True:
            if b * r >= 0.03:
                b0 = b * 0.5
            else:
                f = self.f_list[int(b * r / 0.03 * 10000)]
                F = self.F_list[int(b * r / 0.03 * 10000)]
                f2 = self.f2_list[int(b * r / 0.03 * 10000)]
                nume = (val - b * lda) * r - lda * F / f
                deno = lda * r * (f2 * F / f ** 2 - 2)
                if deno == 0:
                    break
                b0 = b - nume / deno
                if b0 < 0:
                    b0 = b * 1.2
            if abs(b0 - b) < 0.00001 or max_it >= 100:
                break
            b = b0
            max_it += 1
        return (b0 + b) / 2, max_it
