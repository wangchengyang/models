# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import numpy as np


class FCM():
    def __init__(self, mode='dis_sim', dist=1):
        self.mode = mode
        self.dist = dist
        self.list_num = 4
        self.list_len = 10
        self.get_row_exam()

    def get_row_exam(self):
        row_exam = 1 / np.arange(1, self.list_len + 1) ** 0.5
        col_exam = 1 / np.arange(1, self.list_num + 1) ** 0.4
        row_exam = np.tile(np.reshape(row_exam, [1, -1]), [self.list_num, 1])
        col_exam = np.tile(np.reshape(col_exam, [-1, 1]), [1, self.list_len])
        self.exam = row_exam * col_exam

    def get_sim_prob(self, itm1, itm2):
        sim = np.dot(itm1, itm2) / (np.linalg.norm(itm1, axis=-1) * np.linalg.norm(itm2, axis=-1))
        if self.mode == 'sim':
            prob = sim
        else:
            prob = (1 - sim) / 2 + 0.5
        return prob

    def get_neighbor(self, i, j, itms):
        candidate_pos = [(i-1, j), (i, j-1), (i+1, j), (i, j+1)]
        neighbors = []
        for ii, jj in candidate_pos:
            if 0 <= ii < self.list_num and 0 <= jj < self.list_len:
                neighbors.append(itms[ii][jj])
        return neighbors

    def get_cmp_prob(self, cur_itm, neighbors):
        mean_neig = np.mean(np.array(neighbors), axis=0)
        return self.get_sim_prob(mean_neig, cur_itm)

    def generate_page_click(self, itms, lbs):
        orig_shape = lbs.shape
        itms = np.reshape(itms, [self.list_num, self.list_len, -1])
        lbs = np.reshape(lbs, [self.list_num, self.list_len])

        clk_prob, clk = [], []
        for i in range(self.list_num):
            list_click, list_click_prob = [], []
            for j in range(self.list_len):
                rel = lbs[i][j]
                if rel:
                    if sum(itms[i][j]) == 0:
                        print(i, j, lbs[i][j], sum(itms[i][j]))
                    cur_itm = itms[i][j]
                    neighbor = self.get_neighbor(i, j, itms)
                    cmp_ = self.get_cmp_prob(cur_itm, neighbor)
                    click_prob = self.exam[i][j] * cmp_
                    click = 1 if np.random.rand() < click_prob else 0
                    list_click.append(click)
                    list_click_prob.append(click_prob)
                else:
                    list_click.append(0)
                    list_click_prob.append(0)
            clk.append(list_click)
            clk_prob.append(list_click_prob)

        clk = np.array(clk).reshape(orig_shape)
        clk_prob = np.array(clk_prob).reshape(orig_shape)

        return clk.tolist(), clk_prob.tolist()
