# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import json
import time

import numpy as np


def normalize(vec):
    vec = np.array(vec)
    norm = np.linalg.norm(vec)
    if norm == 0:
        return vec
    return vec / norm


def softmax(x):
    x = x - np.max(x)
    exp_x = np.exp(x)
    softmax_x = exp_x / np.sum(exp_x)
    return softmax_x


def get_batch(data, batch_size, batch_no):
    return data[batch_size * batch_no: batch_size * (batch_no + 1)]


def load_parse_from_json(parse, setting_path):
    with open(setting_path, 'r') as f:
        setting = json.load(f)
    parse_dict = vars(parse)
    for k, v in setting.items():
        parse_dict[k] = v
    return parse


def get_reranked_indices(scores, reverse=True):
    indices = list(range(len(scores)))
    reranked_indices = sorted(indices, key=lambda k: scores[k], reverse=reverse)
    return reranked_indices


def get_reranked_batch_ft(fts, lbs, preds):
    rerank_fts, rerank_lbs = [], []
    for per_fts, per_lbs, per_preds in zip(fts, lbs, preds):
        rerank_per_ft, rerank_per_lb = [], []
        for ft, lb, pred in zip(per_fts, per_lbs, per_preds):
            rerank_idx = get_reranked_indices(pred)
            rerank_per_ft.append(np.array(ft)[rerank_idx])
            rerank_per_lb.append(np.array(lb)[rerank_idx])
        rerank_fts.append(rerank_per_ft)
        rerank_lbs.append(rerank_per_lb)

    return np.array(rerank_fts), np.array(rerank_lbs)


def batch_rel_metrics(batch_lbs, batch_preds, is_rank, scopes):
    ndcg, map_ = [[[] for _ in range(len(scopes[i]))] for i in range(len(scopes))], \
                 [[[] for _ in range(len(scopes[i]))] for i in range(len(scopes))]
    for preds, lbs in zip(batch_preds, batch_lbs):
        preds, lbs = preds.tolist(), lbs.tolist()
        for idx, pred, label, scope in zip(range(len(preds)), preds, lbs, scopes):
            if is_rank:
                final = get_reranked_indices(pred)
            else:
                final = list(range(len(pred)))
            click = np.array(label)[final].tolist()  # reranked labels
            gold = get_reranked_indices(label) # optimal list for ndcg

            for i, scop in enumerate(scope):
                ideal_dcg, dcg, ap_value, ap_count = 0, 0, 0, 0
                cur_scope = min(scop, len(label))
                for _i, _g in zip(range(1, cur_scope + 1), gold[:cur_scope]):
                    dcg += (pow(2, click[_i - 1]) - 1) / (np.log2(_i + 1))
                    ideal_dcg += (pow(2, label[_g]) - 1) / (np.log2(_i + 1))

                    if click[_i - 1] >= 1:
                        ap_count += 1
                        ap_value += ap_count / _i

                _ndcg = float(dcg) / ideal_dcg if ideal_dcg != 0 else 0.
                _map_ = float(ap_value) / ap_count if ap_count != 0 else 0.
                ndcg[idx][i].append(_ndcg)
                map_[idx][i].append(_map_)
    return np.array(ndcg), np.array(map_)


def get_distance(item1, item2):
    sim = np.where(item1 == item2, 1, 0)
    return np.mean(sim)


def evaluate_fcm(batches, click_model, page_preds, is_rank):
    print('eval fcm')
    t = time.time()
    scopes = [[10], [10], [10], [10]]
    batch_num = len(batches)
    batch_page_preds = np.split(page_preds, batch_num, axis=0)

    clks, clk_probs = [], []
    for i in range(batch_num):
        itms = batches[i]['dens_ft']
        lbs = batches[i]['lb']

        if is_rank:
            itms, lbs = get_reranked_batch_ft(itms, lbs, batch_page_preds[i])
        for itm, lb in zip(itms, lbs):
            clk, clk_prob = click_model.generate_page_click(itm, lb)
            clks.append(clk)
            clk_probs.append(clk_prob)

    util = np.sum(np.array(clks), axis=-1)

    clk_probs = np.sum(np.array(clk_probs), axis=-1)
    print("generate click TIME: %.4fs" % (time.time() - t))
    t = time.time()

    ndcg, map_ = [], []

    for i in range(batch_num):
        batch_ndcg, batch_map_ = batch_rel_metrics(batches[i]['lb'], batch_page_preds[i], is_rank, scopes)
        ndcg.append(batch_ndcg)
        map_.append(batch_map_)

    ndcg = np.concatenate(ndcg, axis=-1)
    map_ = np.concatenate(map_, axis=-1)
    print("rel & div TIME: %.4fs" % (time.time() - t))

    mean_util_per_list = np.mean(clk_probs, axis=0)
    mean_util = np.mean(np.sum(util, axis=-1))
    mean_clk_prob = np.mean(np.sum(clk_probs, axis=-1))
    mean_ndcg = non_zero_mean(ndcg)
    mean_map_ = non_zero_mean(map_)
    res = {
        'util': mean_util,
        'ctr': mean_clk_prob,
        'ndcg': mean_ndcg,
        'map': mean_map_,
        'util_per_list': mean_util_per_list.tolist()
    }
    return res


def non_zero_mean(arr):
    exist = (arr != 0)
    num = np.sum(arr)
    den = np.sum(exist)
    return num/den
