# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import mindspore as ms
from mindspore.common.initializer import initializer
from mindspore import numpy as mnp
from mindspore import nn, ops
from mindspore_gl.nn import GATConv
from categorical import Categorical
from .BackModels import Transformer

class WithLossCell(nn.Cell):
    def __init__(self, net, morl_weights):
        super(WithLossCell, self).__init__()
        self.net = net
        self.morl_weights = morl_weights

    def construct(self, p_rec, p_target, a_rec, kn_rec, kn_num,
                  b_old_actions, b_old_logprobs, b_old_actionmask, b_rewards):
        logprobs, state_values, dist_entropy = self.net.evaluate(
            p_rec, p_target, a_rec, kn_rec, kn_num, b_old_actions, b_old_actionmask)
        ratios = ops.exp(logprobs - ops.stop_gradient(b_old_logprobs))
        advantages = b_rewards - ops.stop_gradient(state_values)
        advantages = advantages[:, 0] * self.morl_weights[0] + \
                    advantages[:, 1] * self.morl_weights[1] + \
                    advantages[:, 2] * self.morl_weights[2]
        advantages = ops.squeeze(advantages)
        surr1 = ratios * advantages
        surr2 = ops.clip_by_value(ratios, clip_value_min=1-0.2, clip_value_max=1+0.2) * advantages
        actor_loss = -ops.minimum(surr1, surr2).mean()- 0.01 * dist_entropy.mean()
        critic_loss = ops.square(state_values - b_rewards) * 0.5
        critic_loss = critic_loss[:, 0] * self.morl_weights[0] + \
                     critic_loss[:, 1] * self.morl_weights[1] + \
                     critic_loss[:, 2] * self.morl_weights[2]
        critic_loss = critic_loss.mean()
        loss = actor_loss + critic_loss
        return loss

class ActorCritic(nn.Cell):
    def __init__(self, args, local_map):
        super(ActorCritic, self).__init__()
        self.args = args
        self.know_num = args.know_num
        self.item_num = args.item_num
        self.emb_dim = args.emb_dim

        self.directed_g = local_map['directed_g']
        self.k_from_e = local_map['k_from_e']
        self.e_from_k = local_map['e_from_k']
        self.morl_weights = args.morl_weights

        self.knowledge_emb = nn.Embedding(self.know_num, self.emb_dim)
        self.exercise_emb = nn.Embedding(self.item_num, self.emb_dim)# idx=0 pad
        self.ans_embed = nn.Embedding(3, self.emb_dim)
        # all index
        self.k_index = ms.Tensor(list(range(self.know_num)), dtype=ms.int32)
        self.exer_index = ms.Tensor(list(range(self.item_num)), dtype=ms.int32)
        self.graphencoder = Fusion(args, local_map)
        # self attention
        hidden_input_size = 3 * self.emb_dim
        self.encoder = Transformer(hidden_input_size, hidden_input_size, dropout_rate=args.dropout_rate, \
                                    head=args.n_head, b=args.n_block, position=False, transformer_mask=False)
        # actor layer
        self.actor_layer = nn.SequentialCell(
            nn.Dense(hidden_input_size, args.latent_factor),
            nn.Tanh(),
            nn.Dense(args.latent_factor, self.item_num))
        # Accuracy RL Head
        self.acc_head = nn.SequentialCell(
            nn.Dense(hidden_input_size, args.latent_factor),
            nn.Tanh(),
            nn.Dense(args.latent_factor, 1))
        # Diversity RL Head
        self.div_head = nn.SequentialCell(
            nn.Dense(hidden_input_size, args.latent_factor),
            nn.Tanh(),
            nn.Dense(args.latent_factor, 1))
        # Novelty RL Head
        self.nov_head = nn.SequentialCell(
            nn.Dense(hidden_input_size, args.latent_factor),
            nn.Tanh(),
            nn.Dense(args.latent_factor, 1))

        # initialization
        for _, param in self.parameters_and_names():
            if param.dim() > 1:
                param.set_data(initializer('xavier_uniform', param.shape, param.dtype))

    def hidden_layer(self, p_rec, p_target, a_rec, kn_rec, kn_num):
        exer_emb = self.exercise_emb(self.exer_index)
        kn_emb = self.knowledge_emb(self.k_index)
        kn_emb2, exer_emb2 = self.graphencoder(kn_emb, exer_emb)
        batch_exer_emb = exer_emb2[p_rec]
        batch_ans_emb = self.ans_embed(a_rec)
        batch_kn_emb = ops.matmul(kn_rec, kn_emb2) / kn_num.expand_dims(-1)
        item_emb = ops.concat([batch_exer_emb, batch_ans_emb, batch_kn_emb], axis=-1)
        # attention
        src_mask = mask(p_rec, p_target + 1)
        src_mask = src_mask.expand_dims(-2)
        atten_hidden = self.encoder(item_emb, src_mask)
        att_mask = mask(p_rec, p_target + 1)
        atten_hidden = atten_hidden.masked_fill(att_mask.expand_dims(-1) == 0, 0)
        hidden_state = mnp.sum(atten_hidden, axis=1) / mnp.sum(att_mask, axis=-1, keepdims=True)
        return hidden_state

    def construct(self, p_rec, p_target, a_rec, kn_rec, kn_num):
        hidden_state = self.hidden_layer(p_rec, p_target, a_rec, kn_rec, kn_num)
        logits = self.actor_layer(hidden_state)
        return logits

    def evaluate(self, p_rec, p_target, a_rec, kn_rec, kn_num, action, action_mask):
        hidden_state = self.hidden_layer(p_rec, p_target, a_rec, kn_rec, kn_num)
        logits = self.actor_layer(hidden_state)
        action_probs = ops.softmax(logits, axis=-1)
        dist = Categorical(action_probs)
        action_logprobs = dist.log_prob(action)
        dist_entropy = dist.entropy()
        acc_v = self.acc_head(hidden_state)
        div_v = self.div_head(hidden_state)
        nov_v = self.nov_head(hidden_state)
        morl_v = ops.concat([acc_v, div_v, nov_v], axis=1)
        return action_logprobs, morl_v, dist_entropy

class GCAT:
    def __init__(self, args, local_map):
        self.args = args
        self.lr = args.learning_rate
        self.morl_weights = args.morl_weights
        self.eps_clip = 0.2
        self.policy = ActorCritic(args, local_map)
        self.optimizer = nn.Adam(self.policy.trainable_params(), learning_rate=self.lr, beta1=0.9, beta2=0.999)
        self.policy_old = ActorCritic(args, local_map)
        _, _ = ms.load_param_into_net(self.policy_old, self.policy.parameters_dict(), strict_load=False)

    def predict(self, data):
        self.policy_old.set_train(False)
        self.policy_old.set_grad(False)
        p_rec, p_target, a_rec, kn_rec, kn_num = data['p_rec'], \
                    data['p_t'], data['a_rec'], data['kn_rec'], data['kn_num']
        p_rec, p_target, a_rec, kn_rec, kn_num = \
                                ms.Tensor(p_rec, dtype=ms.int32), ms.Tensor(p_target, dtype=ms.int32), \
                                ms.Tensor(a_rec, dtype=ms.int32), kn_rec, kn_num
        logits = self.policy_old(p_rec, p_target, a_rec, kn_rec, kn_num)
        logits = ops.stop_gradient(logits)
        return logits

    def optimize_model(self, data, b_old_actions, b_old_logprobs, b_old_actionmask, b_rewards):
        self.policy.set_train()
        self.policy.set_grad()
        p_rec, p_target, a_rec, kn_rec, kn_num = \
            data['p_rec'], data['p_t'], data['a_rec'], data['kn_rec'], data['kn_num']
        p_rec, p_target, a_rec, kn_rec, kn_num = \
                            ms.Tensor(p_rec, dtype=ms.int32), ms.Tensor(p_target, dtype=ms.int32), \
                            ms.Tensor(a_rec, dtype=ms.int32), kn_rec, kn_num
        network = self.policy
        net_with_loss = WithLossCell(network, self.morl_weights)
        train_net = nn.TrainOneStepCell(net_with_loss, self.optimizer)
        loss = train_net(p_rec, p_target, a_rec, kn_rec, kn_num,
                    b_old_actions, b_old_logprobs, b_old_actionmask, b_rewards)
        return loss.asnumpy()

    def transfer_weights(self):
        _, _ = ms.load_param_into_net(self.policy_old, self.policy.parameters_dict(), strict_load=False)

    @classmethod
    def create_model(cls, config, local_map):
        model = cls(config, local_map)
        return model

class Fusion(nn.Cell):
    def __init__(self, args, local_map):
        super(Fusion, self).__init__()
        self.know_num = args.know_num
        self.item_num = args.item_num
        self.emb_dim = args.emb_dim
        self.directed_g = local_map['directed_g']
        self.k_from_e = local_map['k_from_e']
        self.e_from_k = local_map['e_from_k']

        self.directed_gat = GATConv(in_feat_size=self.emb_dim, out_size=self.emb_dim, num_attn_head=1)
        self.k_from_e_gat = GATConv(in_feat_size=self.emb_dim, out_size=self.emb_dim, num_attn_head=1)
        self.e_from_k_gat = GATConv(in_feat_size=self.emb_dim, out_size=self.emb_dim, num_attn_head=1)

        self.relation_attn = nn.SequentialCell(
            nn.Dense(self.emb_dim, self.emb_dim),
            nn.Tanh(),
            nn.Dense(self.emb_dim, 1, has_bias=False),
        )

    def construct(self, kn_emb, exer_emb):
        k_directed = self.directed_gat(kn_emb, *self.directed_g.get_graph())
        e_k_graph = ops.concat((exer_emb, kn_emb), axis=0)
        k_from_e_graph = self.k_from_e_gat(e_k_graph, *self.k_from_e.get_graph())
        e_from_k_graph = self.e_from_k_gat(e_k_graph, *self.e_from_k.get_graph())
        A = k_directed
        B = k_from_e_graph[self.item_num:]
        score1 = self.relation_attn(A)
        score2 = self.relation_attn(B)
        score = ops.softmax(ops.concat([score1, score2], axis=1), axis=1)
        kn_emb = score[:, 0].unsqueeze(1) * A + score[:, 1].unsqueeze(1) * B
        exer_emb = e_from_k_graph[0: self.item_num]
        return kn_emb, exer_emb

def mask(src, s_len):
    user_mask = ops.zeros_like(src)
    for i in range(len(src)):
        user_mask[i, :int(s_len[i].asnumpy())] = 1
    return user_mask
