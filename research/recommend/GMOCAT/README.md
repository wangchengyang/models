# Contents

- [Contents](#contents)
- [GMOCAT Description](#GMOCAT-description)
- [Dataset](#dataset)
- [Cognitive Diagnosis](#cognitive-diagnosis)
- [Environment Requirements](#environment-requirements)
- [Quick Start](#quick-start)
- [Script Description](#script-description)
    - [Script and Sample Code](#script-and-sample-code)
    - [Script Parameters](#script-parameters)
- [Model Description](#model-description)
    - [Performance](#performance)
        - [Training Performance](#training-performance)
        - [Inference Performance](#inference-performance)
- [Description of Random Situation](#description-of-random-situation)
- [ModelZoo Homepage](#modelzoo-homepage)

# [GMOCAT Description](#contents)

The proposed model, GMOCAT, introduces three objectives, namely quality, diversity and novelty, into the Scalarized Multi-Objective Reinforcement Learning framework of CAT, which respectively correspond to improving the prediction accuracy, increasing the concept diversity and reducing the question exposure. An Actor-Critic Recommender is used to select questions and optimize three objectives simultaneously by the scalarization function. Meanwhile, the graph neural network are utilized to learn relation-aware embeddings of questions and concepts. These embeddings are able to aggregate neighborhood information in the relation graphs between questions and concepts.

GMOCAT: A Graph-Enhanced Multi-Objective Method for Computerized Adaptive Testing

KDD2023

# [Dataset](#contents)

- [Assist2009](https://sites.google.com/site/assistmentsdata/home/2009-2010-assistment-data)

# [Cognitive Diagnosis](#contents)

- IRT

# [Environment Requirements](#contents)

- Hardware（CPU）
    - Prepare hardware environment with CPU  processor.
- Framework
    - [MindSpore-2.0.0](https://www.mindspore.cn/install/en)
- Requirements
  - pandas
  - numpy
  - logging
  - mindspore==2.0.0
  - mindspore-gl==0.2
  - tqdm
  - sklearn
- For more information, please check the resources below：
  - [MindSpore Tutorials](https://www.mindspore.cn/tutorials/en/r2.0/index.html)
  - [MindSpore Python API](https://www.mindspore.cn/docs/en/r2.0/index.html)

# [Quick Start](#contents)

After installing MindSpore via the official website, you can start training and evaluation as follows:

- running on CPU

  ```python
  # get calibrated question parameters
  python pretrain.py
  # run training and evaluation
  python train_gcat.py
  ```

# [Script Description](#contents)

## [Script and Sample Code](#contents)

```text
.
└─GMOCAT
  ├─README.md             # descriptions of warpctc
  ├─agents                # different agents
    ├─__init__.py
    ├─dataset.py          # dataset to get batch students
    ├─GCATAgent.py        # GMOCAT agent
  ├─envs                  # different envs
    ├─__init__.py
    ├─dataset.py          # dataset to get student id, question id
    ├─Env.py              # Base Env
    ├─GCATEnv.py          # GMOCAT Env
    ├─irt.py              # irt model
    ├─ncd.py              # ncd model
  ├─function              # different selection algorithms
    ├─__init__.py
    ├─BackModels.py       # basic models
    ├─GCAT.py             # model structure for selection algorithm in GMOCAT
  ├─categorical.py
  ├─launch_gcat.py        # main function to run GMOCAT
  ├─pretrain.py           # main function to get calibrated question parameters
  └─util.py               # utils
```

## [Script Parameters](#contents)

Parameters for getting calibrated question parameters can be found in `pretrain.py`

Parameters for both training and evaluation can be found in `launch_gcat.py`

# [Model Description](#contents)

## [Performance](#contents)

### Training Performance

| Parameters          | CPU                               |
|---------------------|-----------------------------------|
| Model Version       | GMOCAT                              |
| Resource            | CPU 2.90GHz;16Core;32G Memory     |
| uploaded Date       | 08/03/2023 (month/day/year)       |
| MindSpore Version   | 2.0.0                             |
| Dataset             | Assist2009                               |
| Training Parameters | epoch=50, batch_size=128, lr=1e-3 |
| Optimizer           | Adam                              |
| Loss Function       | PPO loss |
| outputs             | Loss                              |
| Loss                | 0.5248                             |
| Per Step Time       | 3000 ms                          |

### Inference Performance

| Parameters        | CPU                           |
|-------------------|-------------------------------|
| Model Version     | GMOCAT                          |
| Resource          | CPU 2.90GHz;16Core;32G Memory |                        |
| Uploaded Date     | 08/03/2023 (month/day/year)   |
| MindSpore Version | 2.0.0                         |
| Dataset           |Assist2009                           |
| batch_size        | 1024                          |
| outputs           | AUC                           |
| AUC               | 0.7221                         |

# [Description of Random Situation](#contents)

- We set the random seed before training.
- Random initialization of model weights

# [ModelZoo Homepage](#contents)

 Please check the official [homepage](https://gitee.com/mindspore/models)