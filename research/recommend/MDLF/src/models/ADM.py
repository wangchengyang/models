# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import mindspore.ops as ops
import mindspore.nn as nn


class ADM(nn.Cell):
    def __init__(self, feature_num, embedding_size=8, intervals=100, length=5, MAX_SPARSE=110):
        super(ADM, self).__init__()

        self.embedding_size = embedding_size
        self.embeddings = nn.Embedding(MAX_SPARSE, self.embedding_size, embedding_table='normal')

        self.K = length
        self.N = intervals
        self.feature_num = feature_num

        self.share_layers = nn.SequentialCell([
            nn.Dense(self.feature_num * self.embedding_size, self.N // 4),
            nn.ReLU(),
            nn.Dense(self.N // 4, self.N // 2)
        ])

        self.last_layer = nn.Dense(self.N // 2 + self.feature_num * self.embedding_size, self.K * self.N)

    def construct(self, x):
        embs = self.embeddings(x).reshape(-1, self.feature_num * self.embedding_size)
        high = self.share_layers(embs)
        output = self.last_layer(ops.Concat(axis=1)([embs, high])).reshape(-1, self.K, self.N)
        pdf = ops.Softmax(axis=-1)(output)
        cdf = ops.CumSum()(pdf, 2)
        winning_rate = ops.Concat(axis=1)((cdf[:, 0:1, :], cdf[:, 1:, :] - cdf[:, :-1, :]))
        return pdf, cdf, winning_rate
