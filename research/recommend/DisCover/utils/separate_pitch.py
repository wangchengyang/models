# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import os
from multiprocessing import Pool
from tqdm import tqdm
import numpy as np
import parselmouth


SAMPLE_RATE = 22050
F0_MIN = 80
F0_MAX = 750
HOP_SIZE = 512
TIME_STEP = HOP_SIZE / SAMPLE_RATE * 1000
IN_DATA = ""
OUT_DATA = ""
all_files = []
waved_files = []
remain_files = []
separated_pitch_shs100k = {}

for idx, (root, dirs, files) in tqdm(enumerate(os.walk(IN_DATA))):
    files_num = len(files)
    if files_num > 0:
        for file in files:
            in_path = os.path.join(root, file)
            set_id = root.split("/")[-1]
            out_path = OUT_DATA + file.split(".")[0] + ".npy"
            all_files.append((in_path, out_path))
print(len(all_files), all_files[:5])


def separate(args):
    _in_path, _out_path = args
    f0 = (
        parselmouth.Sound(_in_path)
        .to_pitch_ac(
            TIME_STEP=TIME_STEP / 1000,
            voicing_threshold=0.6,
            pitch_floor=F0_MIN,
            pitch_ceiling=F0_MAX,
        )
        .selected_array["frequency"]
    )
    np.save(_out_path, f0)
    print(_out_path)

if __name__ == "__main__":
    print("begin")
    pool = Pool(40)
    pool.map(separate, all_files)
    pool.close()
    pool.join()
    print("end")
