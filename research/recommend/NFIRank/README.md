# Contents

- [Contents](#contents)
- [NFIRank Description](#nfirank-description)
- [Dataset](#dataset)
- [Environment Requirements](#environment-requirements)
- [Quick Start](#quick-start)
- [Script Description](#script-description)
    - [Script and Sample Code](#script-and-sample-code)
    - [Script Parameters](#script-parameters)
- [Model Description](#model-description)
    - [Performance](#performance)
        - [Training Performance](#training-performance)
- [Description of Random Situation](#description-of-random-situation)
- [ModelZoo Homepage](#modelzoo-homepage)

# [NFIRank Description](#contents)

The proposed model, NFIRank, formulates the news recommendation session as a MDP and updates the model with TD (Temporal Difference) losses by Q-learning. Additionally, to prove the stability of the policy and keep the video exposure rate within a proper range, L_2 regularization loss and KL loss are added to the model.

Integrated Ranking for News Feed with Reinforcement Learning

WWW2023

# [Dataset](#dataset)

- This paper does not include any results in public datasets, so we provide a fake data generator.

# [Environment Requirements](#contents)

- Hardware（CPU）
    - Prepare hardware environment with CPU processor.
- Framework
    - [MindSpore-2.0.0-alpha](https://www.mindspore.cn/install/en)
- Requirements
    - pandas
    - numpy
    - logging
    - mindspore==2.0.0-alpha
    - tqdm
- For more information, please check the resources below：
    - [MindSpore Tutorials](https://www.mindspore.cn/tutorials/en/r2.0.0-alpha/index.html)
    - [MindSpore Python API](https://www.mindspore.cn/docs/en/r2.0.0-alpha/index.html)

# [Quick Start](#contents)

After installing MindSpore via the official website, you can start training and evaluation as follows:

- running on CPU

  ```python
  # generate fake data
  mkdir ./data
  python fake_data_generator.py
  # run training evaluation
  python train.py
  ```

# [Script Description](#contents)

## [Script and Sample Code](#contents)

```text
.
└── NFIRank
    ├── README.md               # Descriptions
    ├── algorithm.py            # Main algorithm and model
    ├── config.py               # configs
    ├── data_generator.py       # Main function of datasets
    ├── fake_data_generator.py  # A generator for generating fake data
    └── train.py                # Main function to run NFIRank
```

## [Script Parameters](#contents)

Parameters for both fake data generating and training can be found in `config.py`

# [Model Description](#contents)

## [Performance](#contents)

### Training Performance

| Parameters          | CPU                               |
| ------------------- | --------------------------------- |
| Model Version       | NFIRank                           |
| Resource            | CPU 5.1GHz;16Core;32G Memory      |
| uploaded Date       | 09/16/2023 (month/day/year)       |
| MindSpore Version   | 2.0.0a0                           |
| Dataset             | Fake data                         |
| Training Parameters | epoch=50, batch_size=256, lr=1e-3 |
| Optimizer           | Adam                              |
| Loss Function       | Q loss                            |
| outputs             | Loss                              |
| Loss                | 0.5248                            |
| Per Step Time       | 3000 ms                           |

# [Description of Random Situation](#contents)

- We set the random seed before training.
- Random initialization of model weights

# [ModelZoo Homepage](#contents)

 Please check the official [homepage](https://gitee.com/mindspore/models)