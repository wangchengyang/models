# Copyright 2021-2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
"""
RetinaNet model
(base architecture with ResNet backbone, FPN neck and dense head).
"""

import numpy as np
from tqdm import tqdm

import mindspore as ms
from mindspore import nn, ops
from mindspore.ops import functional as F
from mindspore.ops import operations as P

from .anchor_generator import AnchorGenerator
from .base_dense_head import BaseDenseHead
from .bbox_coder import DeltaXYWHBBoxCoder
from .bbox_overlaps import bbox_overlaps_ms
from .fpn import FPN
from .multi_box import MultiBox
from .backbone.resnet import ResNet


class RetinanetWithLossCell(nn.Cell):
    """
    Provide retinanet training loss through network.

    Args:
        network (Cell): The training network.
        config (dict): retinanet config.

    Returns:
        Tensor, the loss of the network.
    """

    def __init__(self, network, config):
        super(RetinanetWithLossCell, self).__init__()
        self.network: RetinaNet50 = network
        self.less = P.Less()
        self.tile = P.Tile()
        self.reduce_sum = P.ReduceSum()
        self.reduce_mean = P.ReduceMean()
        self.expand_dims = P.ExpandDims()
        self.num_classes = config.bbox_head.num_classes
        self.cls_out_channels = self.num_classes  # sigmoid
        self.cast = P.Cast()

        self.eps = 1e-12
        self.pre_anchor_topk = 50

        self.anchor_generator = AnchorGenerator(
            strides=config.bbox_head.anchor_generator.strides,
            ratios=config.bbox_head.anchor_generator.ratios,
            octave_base_scale=config.bbox_head.anchor_generator.octave_base_scale,
            scales_per_octave=config.bbox_head.anchor_generator.scales_per_octave
        )

        self.featmap_sizes = [[x, x] for x in config.feature_size]
        self.bbox_thr = config.bbox_head.assigner.pos_iou_thr

    def get_multi_level_anchors(self, featmap_sizes):
        return [
            prior_generator for prior_generator, featmap_size in zip(
                self.anchor_generator.grid_priors(featmap_sizes), featmap_sizes
            )
        ]

    def get_anchors(self, featmap_sizes, img_metas):
        """Get anchors according to feature map sizes.

        Args:
            featmap_sizes (list[tuple]): Multi-level feature map sizes.
            img_metas (list[dict]): Image meta info.

        Returns:
            tuple:
                anchor_list (list[Tensor]): Anchors of each image.
                valid_flag_list (list[Tensor]): Valid flags of each image.
        """
        num_imgs = len(img_metas)

        # since feature map sizes of all images are the same, we only compute
        # anchors for one time
        multi_level_anchors = self.get_multi_level_anchors(featmap_sizes)
        anchor_list = [multi_level_anchors for _ in range(num_imgs)]

        # for each image, we compute valid flags of multi level anchors
        valid_flag_list = []
        for img_meta in img_metas:
            multi_level_flags = [
                prior_generator.valid_flags(
                    featmap_size, img_meta['pad_shape'])
                for prior_generator, featmap_size in zip(
                    self.prior_generators, featmap_sizes
                )
            ]
            valid_flag_list.append(multi_level_flags)

        return anchor_list, valid_flag_list

    def construct(self, *args):
        def transpose_concat(src, num_channels):
            return ops.concat([
                ops.transpose(
                    elem, (0, 2, 3, 1)
                ).reshape(elem.shape[0], -1, num_channels)
                for elem in src
            ], axis=1)

        x, _, gt_loc, gt_label, _ = args
        bbox_preds, cls_scores = self.network(x)
        cls_scores = transpose_concat(cls_scores, self.cls_out_channels)
        bbox_preds = transpose_concat(bbox_preds, 4)
        cls_prob = ops.sigmoid(cls_scores)
        anchors = self.get_multi_level_anchors(self.featmap_sizes)
        anchors = np.concatenate(anchors, axis=0)

        positive_losses, box_prob, num_pos = [], [], 0
        for k in range(bbox_preds.shape[0]):
            gt_bbox, gt_cls = gt_loc[k], gt_label[k, :, 0]
            pd_bbox, pd_cls = bbox_preds[k], cls_prob[k]
            pd_bbox = ops.reshape(pd_bbox, (-1, 4))
            num_obj = gt_cls.shape[0]
            if num_obj == 0:
                image_box_prob = ops.zeros(
                    (anchors.shape[0], self.cls_out_channels), bbox_preds.dtype)
            else:
                pd_iou = bbox_overlaps_ms(gt_bbox, pd_bbox)
                t1 = self.bbox_thr
                t2, _ = ops.max(pd_iou, axis=1, keep_dims=True)
                t2 = ops.clip_by_value(t2, ms.Tensor(t1 + 1e-12))

                object_box_prob = (pd_iou - t1) / (t2 - t1)  # clamp(0, 1)
                object_box_prob = ops.clip_by_value(
                    object_box_prob, ms.Tensor(0), ms.Tensor(1))
                object_cls_box_prob = ops.zeros(
                    (num_obj, self.cls_out_channels, object_box_prob.shape[1]),
                    ms.float32
                )
                for m in range(gt_cls.shape[0]):
                    ind0, ind1 = m, gt_cls[m].asnumpy().item() - 1
                    object_cls_box_prob[ind0, ind1, :] = object_box_prob[m, :]
                box_cls_prob = ops.reduce_sum(object_cls_box_prob, axis=0)

                indices = ops.nonzero(box_cls_prob)[:100, :]  # k, 2
                if indices.shape[0] == 0:
                    image_box_prob = ops.zeros((anchors.shape[0], self.cls_out_channels),
                                               bbox_preds.dtype)
                else:
                    selected_probs, indices_np = [], indices.asnumpy()
                    for m in tqdm(range(indices.shape[0])):
                        ind = indices_np[m, 1].item()#.asnumpy().item()
                        selected_probs.append(object_box_prob[:, ind])
                    mask = (ops.expand_dims(gt_cls, -1) == indices[:, 0]
                            ).transpose((1, 0)).copy()
                    selected_probs = ops.concat(selected_probs).reshape(
                        (indices.shape[0], object_box_prob.shape[0])).copy()
                    nonzero_box_prob = mask.astype(ms.int32).choose(
                        [selected_probs,
                         ms.Tensor([[0]], ms.float32)]
                    )
                    nonzero_box_prob = ops.max(nonzero_box_prob, axis=0)[1]  # values
                    image_box_prob = ops.zeros(
                        (anchors.shape[0], self.cls_out_channels), ms.float32)
                    image_box_prob = ops.tensor_scatter_elements(
                        image_box_prob,
                        indices[::-1], nonzero_box_prob,
                        0, 'add'
                    )
            box_prob.append(image_box_prob)

            _, matched = ops.top_k(
                bbox_overlaps_ms(gt_bbox, ms.Tensor(anchors)),
                self.pre_anchor_topk, sorted=False)
            matched_cls_prob = ops.gather_d(
                pd_cls[matched], 2,
                gt_cls.view((-1, 1, 1)).repeat((1, self.pre_anchor_topk, 1))
            ).squeeze(2)  # P_{ij}^{cls}
            matched_anchors = anchors[matched]
            matched_object_targets = self.bbox_coder.encode(
                matched_anchors,
                gt_bbox.unsqueeze(dim=1).expand_as(matched_anchors))
            loss_bbox = self.loss_bbox(pd_bbox[matched], matched_object_targets,
                                       reduction_override='none').sum(-1)
            matched_box_prob = ops.exp(-loss_bbox)  # P_{ij}^{loc}
            num_pos += len(gt_bbox)
            positive_losses.append(  # {-log( Mean-max(P_{ij}^{cls} * P_{ij}^{loc}) )}
                self.positive_bag_loss(matched_cls_prob, matched_box_prob))

        positive_loss = ops.concat(positive_losses).sum() / max(1, num_pos)
        box_prob = ops.stack(box_prob, axis=0)  # box_prob: P{a_{j} \in A_{+}}
        # \sum_{j}{ FL((1 - P{a_{j} \in A_{+}}) * (1 - P_{j}^{bg})) } / n||B||
        negative_loss = self.negative_bag_loss(cls_prob, box_prob).sum() / max(
            1, num_pos * self.pre_anchor_topk)
        if num_pos == 0:  # empty GT, but gradients are needed
            positive_loss = bbox_preds.sum() * 0
        return positive_loss + negative_loss

    def positive_bag_loss(self, matched_cls_prob, matched_box_prob):
        # bag_prob = Mean-max(matched_prob)
        matched_prob = matched_cls_prob * matched_box_prob
        weight = 1 / ops.clip_by_value(1 - matched_prob, ms.tensor(1e-12))
        weight /= weight.sum(dim=1).unsqueeze(dim=-1)
        bag_prob = (weight * matched_prob).sum(dim=1)
        # positive_bag_loss = -self.alpha * log(bag_prob)
        return self.alpha * F.binary_cross_entropy(
            bag_prob, ops.ones_like(bag_prob), reduction='none')

    def negative_bag_loss(self, cls_prob, box_prob):
        prob = cls_prob * (1 - box_prob)
        # There are some cases when neg_prob = 0.
        # This will cause the neg_prob.log() to be inf without clamp.
        prob = prob.clamp(min=self.eps, max=1 - self.eps)
        negative_bag_loss = prob**self.gamma * F.binary_cross_entropy_with_logits(
            prob, ops.zeros_like(prob),
            ops.ones_like(prob), ops.ones_like(prob),
            reduction='none'
        )
        return (1 - self.alpha) * negative_bag_loss


class RetinaNet50(nn.Cell):
    def __init__(self, backbone, config, is_training=True):
        super(RetinaNet50, self).__init__()
        self.backbone: ResNet = backbone
        self.P7_1 = nn.ReLU()
        feature_shapes = [
            [config.img_height // 4, config.img_width // 4],
            [config.img_height // 8, config.img_width // 8],
            [config.img_height // 16, config.img_width // 16],
            [config.img_height // 32, config.img_width // 32],
            [config.img_height // 64, config.img_width // 64],
        ]

        # Neck
        self.neck = FPN(
            in_channels=config.neck.fpn.in_channels,
            out_channels=config.neck.fpn.out_channels,
            start_level=config.neck.fpn.start_level,
            num_outs=config.neck.fpn.num_outs,
            add_extra_convs=config.neck.fpn.add_extra_convs,
            feature_shapes=feature_shapes
        )

        self.bbox_head = MultiBox(config)
        self.is_training = is_training
        if not is_training:
            self.activation = P.Sigmoid()

    def construct(self, x):
        x = self.backbone(x)
        multi_feature = self.neck(x)
        pred_loc, pred_label = self.bbox_head(multi_feature)

        return pred_loc, pred_label


class RetinanetInferWithDecoder(BaseDenseHead):
    """
    retinanet Infer wrapper to decode the bbox locations.

    Args:
        network (Cell): the origin retinanet infer network without bbox decoder.
        default_boxes (Tensor): the default_boxes from anchor generator
        config (dict): retinanet config
    Returns:
        Tensor, the locations for bbox after decoder representing (y0,x0,y1,x1)
        Tensor, the prediction labels.

    """
    def __init__(self, network, config):
        super(RetinanetInferWithDecoder, self).__init__()
        self.dtype = np.float32
        self.network = network
        self.coder = DeltaXYWHBBoxCoder(
            target_means=config.bbox_head.bbox_coder.target_means,
            target_stds=config.bbox_head.bbox_coder.target_stds
        )

        self.anchor_generator = AnchorGenerator(
            strides=config.bbox_head.anchor_generator.strides,
            ratios=config.bbox_head.anchor_generator.ratios,
            octave_base_scale=config.bbox_head.anchor_generator.octave_base_scale,
            scales_per_octave=config.bbox_head.anchor_generator.scales_per_octave
        )

        self.test_cfg = config.test_cfg
        self.cls_out_channels = config.bbox_head.num_classes
        self.use_sigmoid_cls = config.bbox_loss_cls_use_sigmoid

    def construct(self, x):
        bbox_preds, cls_scores = self.network(x)
        return bbox_preds, cls_scores
