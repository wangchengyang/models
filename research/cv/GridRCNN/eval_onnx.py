# Copyright 2022-2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Evaluation of ONNX model"""
import argparse
import os
from pprint import pprint

import onnxruntime as ort
from mindspore.common import set_seed
from tqdm import tqdm

from src.dataset import create_mindrecord_dataset
from src.model_utils.config import (
    parse_yaml, parse_cli_to_yaml, merge, Config
)
from src.eval_utils import COCOMeanAveragePrecision


def get_config():
    """
    Get Config according to the yaml file and cli arguments.
    """
    parser = argparse.ArgumentParser(description='default name',
                                     add_help=False)
    current_dir = os.path.dirname(os.path.abspath(__file__))
    parser.add_argument('--config_path', type=str,
                        default=os.path.join(current_dir, 'configs',
                                             'default_config.yaml'),
                        help='Config file path')
    parser.add_argument('--onnx_path', help='Path to save checkpoint.')
    parser.add_argument('--eval_results_path', default='eval_results',
                        help='Path to folder with evaluation results.')
    parser.add_argument('--prediction_path', help='Path to model predictions.')
    path_args, _ = parser.parse_known_args()
    default, helper, choices = parse_yaml(path_args.config_path)
    args = parse_cli_to_yaml(parser=parser, cfg=default, helper=helper,
                             choices=choices, cfg_path=path_args.config_path)
    final_config = Config(merge(args, default))
    pprint(final_config)
    print('Please check the above information for the configurations',
          flush=True)
    return final_config


def create_session(checkpoint_path, target_device):
    """Create ONNX runtime session"""
    if target_device == 'GPU':
        providers = ['CUDAExecutionProvider']
    elif target_device in ('CPU', 'Ascend'):
        providers = ['CPUExecutionProvider']
    else:
        raise ValueError(
            f"Unsupported target device '{target_device}'. "
            f"Expected one of: 'CPU', 'GPU', 'Ascend'"
        )
    session = ort.InferenceSession(checkpoint_path, providers=providers)
    input_names = [x.name for x in session.get_inputs()]
    return session, input_names


def eval_gridrcnn(ds, ckpt_path, anno_path, target_device):
    """GridRcnn evaluation."""
    if not os.path.isfile(ckpt_path):
        raise RuntimeError(f'CheckPoint file {ckpt_path} is not valid.')
    session, input_names = create_session(ckpt_path, target_device)

    eval_iter = 0
    total = ds.get_dataset_size()
    metric = COCOMeanAveragePrecision(anno_path, bbox_normalization=False)
    print('\n========================================\n')
    print('total images num: ', total)
    print('Processing, please wait a moment.')

    if os.path.exists(config.prediction_path):
        print(f'Load predictions from file: {config.prediction_path}')
        metric.load_preds(config.prediction_path)
    else:
        for data in tqdm(
                ds.create_dict_iterator(num_epochs=1),
                total=ds.get_dataset_size()
        ):
            eval_iter = eval_iter + 1

            img_data = data['image'].asnumpy().astype('float32')
            img_metas = data['image_shape'].asnumpy().astype('float32')
            gt_bboxes = data['box'].asnumpy().astype('float32')
            gt_labels = data['label'].asnumpy()
            gt_num = data['valid_num'].asnumpy()

            img_shapes = img_metas[::, :-1]
            input_data = [img_data, img_shapes]

            output = session.run(None, dict(zip(input_names, input_data)))
            all_bbox = output[0]
            all_label = output[1]
            all_mask = output[2]
            metric.update(
                (all_bbox, all_label, all_mask),
                (gt_bboxes, gt_labels, gt_num, img_metas)
            )

        if config.prediction_path is not None:
            metric.dump_preds(config.prediction_path)

    print(f'Eval result: {metric.eval()}')
    print("\nEvaluation done!")


def main():
    """Main function"""
    set_seed(1)
    val_dataset = create_mindrecord_dataset(
        path=config.val_dataset, config=config, training=False,
        python_multiprocessing=config.python_multiprocessing
    )
    print("CHECKING MINDRECORD FILES DONE!")
    print("Start Eval!")
    eval_gridrcnn(
        val_dataset, config.onnx_path,
        os.path.join(config.val_dataset, 'labels.json'),
        config.device_target
    )


if __name__ == '__main__':
    config = get_config()
    main()
