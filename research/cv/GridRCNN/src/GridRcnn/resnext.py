# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the https://github.com/STVIR/Grid-R-CNN
# repository and modified.
# ============================================================================
"""ResNext backbone"""
import math

from mindspore import nn

from .resnet import ResNet


class Bottleneck(nn.Cell):

    expansion = 4

    def __init__(
            self,
            inplanes,
            planes,
            stride=1,
            dilation=1,
            downsample=None,
            norm_eval=False,
            weights_update=False,
            groups=1,
            base_width=4
    ):
        """Bottleneck block for ResNeXt.
        If style is "pytorch", the stride-two layer is the 3x3 conv layer,
        if it is "caffe", the stride-two layer is the first 1x1 conv layer.
        """
        super(Bottleneck, self).__init__()
        self.inplanes = inplanes
        self.planes = planes
        self.stride = stride
        self.dilation = dilation
        self.conv1_stride = 1
        self.conv2_stride = stride

        self.weights_update = weights_update
        self.norm_eval = norm_eval
        self.affine = weights_update

        if groups == 1:
            width = self.planes
        else:
            width = math.floor(self.planes * (base_width / 64)) * groups

        self.bn1 = nn.BatchNorm2d(width, affine=self.affine)
        self.bn2 = nn.BatchNorm2d(width, affine=self.affine)
        self.bn3 = nn.BatchNorm2d(
            self.planes * self.expansion, affine=self.affine
        )

        self.conv1 = nn.Conv2d(
            self.inplanes,
            width,
            kernel_size=1,
            stride=self.conv1_stride,
            has_bias=False
        )
        self.conv2 = nn.Conv2d(
            width,
            width,
            kernel_size=3,
            stride=self.conv2_stride,
            padding=self.dilation,
            pad_mode='pad',
            dilation=self.dilation,
            has_bias=False,
            group=groups,
        )
        self.conv3 = nn.Conv2d(
            width,
            self.planes * self.expansion,
            kernel_size=1,
            has_bias=False
        )

        self.relu = nn.ReLU()
        self.downsample = downsample

        if not self.weights_update:
            self.conv1.weight.requires_grad = False
            self.conv2.weight.requires_grad = False
            self.conv3.weight.requires_grad = False
            if self.downsample is not None:
                self.downsample[0].weight.requires_grad = False

        if self.norm_eval:
            self.bn1 = self.bn1.set_train(False)
            self.bn2 = self.bn2.set_train(False)
            self.bn3 = self.bn3.set_train(False)
            if self.downsample is not None:
                self.downsample[1].set_train(False)

    def construct(self, x):
        """run forward"""
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            identity = self.downsample(x)
        out += identity
        out = self.relu(out)

        return out

    def set_train(self, mode=True):
        """set train mode."""
        super(Bottleneck, self).set_train(mode=mode)
        self.bn1.set_train(mode and (not self.norm_eval))
        self.bn2.set_train(mode and (not self.norm_eval))
        self.bn3.set_train(mode and (not self.norm_eval))
        if self.downsample is not None:
            self.downsample[1].set_train(
                mode and (not self.norm_eval)
            )


class ResNeXt(ResNet):
    """ResNeXt backbone.

    Args:
        depth (int): Depth of resnet, from {18, 34, 50, 101, 152}.
        num_stages (int): Resnet stages, normally 4.
        groups (int): Group of resnext.
        base_width (int): Base width of resnext.
        strides (Sequence[int]): Strides of the first block of each stage.
        dilations (Sequence[int]): Dilation of each stage.
        out_indices (Sequence[int]): Output from which stages.
        frozen_stages (int): Stages to be frozen (all param fixed). -1 means
            not freezing any parameters.
        norm_eval (bool): Whether to set norm layers to eval mode, namely,
            freeze running stats (mean and var). Note: Effect on Batch Norm
            and its variants only.
    """

    arch_settings = {
        50: (Bottleneck, (3, 4, 6, 3)),
        101: (Bottleneck, (3, 4, 23, 3)),
        152: (Bottleneck, (3, 8, 36, 3))
    }

    def __init__(self, groups=1, base_width=4, **kwargs):
        super(ResNeXt, self).__init__(**kwargs)
        self.groups = groups
        self.base_width = base_width

        self.inplanes = 64
        self.res_layers = nn.CellList()
        for i, num_blocks in enumerate(self.stage_blocks):
            stride = self.strides[i]
            dilation = self.dilations[i]
            planes = 64 * 2 ** i
            res_layer = make_res_layer(
                self.block,
                self.inplanes,
                planes,
                num_blocks,
                stride=stride,
                dilation=dilation,
                groups=self.groups)
            self.inplanes = planes * self.block.expansion
            self.res_layers.append(res_layer)

        self.feat_dim = self.block.expansion * 64 * 2 ** (
            len(self.stage_blocks) - 1
        )





def make_res_layer(
        block,
        inplanes,
        planes,
        blocks,
        stride=1,
        dilation=1,
        groups=1,
        weights_update=True,
        norm_eval=False
):
    downsample = None
    if stride != 1 or inplanes != planes * block.expansion:
        downsample = nn.SequentialCell(
            nn.Conv2d(
                inplanes,
                planes * block.expansion,
                kernel_size=1,
                stride=stride,
                has_bias=False),
            nn.BatchNorm2d(planes * block.expansion),
        )

    layers = []
    layers.append(
        block(
            inplanes,
            planes,
            stride,
            dilation,
            downsample,
            norm_eval=norm_eval,
            weights_update=weights_update,
            groups=groups
        )
    )
    inplanes = planes * block.expansion
    for _ in range(1, blocks):
        layers.append(
            block(
                inplanes,
                planes,
                dilation=dilation,
                stride=1,
                downsample=None,
                norm_eval=norm_eval,
                weights_update=weights_update,
                groups=groups
            )
        )

    return nn.SequentialCell(*layers)
