import os
import mindspore
import mindspore.nn as nn

#import models
from .model_registry import is_model, model_entrypoint

def split_weights(net):
    decay = []
    no_decay = []

    for m in net.modules():
        if isinstance(m, (nn.Conv2d, nn.Linear)):
            decay.append(m.weight)

            if m.bias is not None:
                no_decay.append(m.bias)

        else:
            if hasattr(m, 'weight') and m.weight is not None:
                no_decay.append(m.weight)
            if hasattr(m, 'bias') and m.bias is not None:
                no_decay.append(m.bias)
    assert len(list(net.parameters())) == len(decay) + len(no_decay)

    return [dict(params=decay), dict(params=no_decay, weight_decay=0)]


def create_model(args):
    if is_model(args.arch):
        model = model_entrypoint(args.arch)(args)
    return model


def resume_model(model, args):
    # optionally resume from a checkpoint
    print(args.resume_checkpoint)
    if os.path.exists(args.resume_checkpoint):
        print("=> loading checkpoint '{}'".format(args.resume_checkpoint))
        checkpoint = mindspore.load_checkpoint(args.resume_checkpoint, net=model)
        # print(checkpoint)
        # args.start_epoch = checkpoint['epoch']
        # best_acc1 = checkpoint['best_acc1']

        # model.load_param_into_net(checkpoint['state_dict'])
        # optimizer.load_param_into_net(checkpoint['optimizer'])
        mindspore.load_param_into_net(model, checkpoint)
        print("=> loaded checkpoint '{}'".format(args.resume_checkpoint))

        # base_lr = optimizer.param_groups[-1]['lr']
        # optimizer = nn.SGD(model.parameters(),base_lr,momentum=args.momentum,weight_decay=args.weight_decay)

        return model
    print("=> no checkpoint found at '{}'".format(args.resume))
    return None
