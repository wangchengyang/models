# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# ============================================================================

"""Foveabox"""

import numpy as np
import mindspore as ms
from mindspore import nn

from .resnet import ResNet
from .fpn import FPN
from .bbox_head import FoveaHead
from .loss import SigmoidFocalClassificationLoss


class Foveabox(nn.Cell):
    """
    Foveabox Network.

    Examples:
        net = Foveabox(config)
    """

    def __init__(self, config):
        super().__init__()
        self.dtype = np.float32
        self.ms_type = ms.float32
        self.train_batch_size = config.batch_size
        self.test_batch_size = config.test_batch_size
        self.max_num = config.num_gts

        # Backbone
        self.backbone = self.create_backbone(config)

        # Neck
        self.neck = FPN(
            in_channels=config.neck.fpn.in_channels,
            out_channels=config.neck.fpn.out_channels,
            start_level=config.neck.fpn.start_level,
            num_outs=config.neck.fpn.num_outs,
            add_extra_convs=config.neck.fpn.add_extra_convs,
            feature_shapes=config.feature_shapes
        )

        loss_cls = SigmoidFocalClassificationLoss(
            gamma=config.bbox_head.loss_cls.get('gamma', 1.5),
            alpha=config.bbox_head.loss_cls.get('alpha', 0.4),
        )
        loss_bbox = nn.SmoothL1Loss(
            beta=config.bbox_head.loss_bbox.get('beta', 0.11),
            reduction='mean'
        )

        self.bbox_head = FoveaHead(
            num_classes=config.bbox_head.num_classes,
            in_channels=config.bbox_head.in_channels,
            img_width=config.img_width,
            img_height=config.img_height,
            batch_size=config.batch_size,
            loss_cls=loss_cls,
            loss_bbox=loss_bbox,
            strides=config.bbox_head.strides,
            base_edge_list=config.bbox_head.base_edge_list,
            scale_ranges=config.bbox_head.scale_ranges,
            sigma=config.bbox_head.sigma,
            with_deform=config.bbox_head.with_deform,
            stacked_convs=config.bbox_head.stacked_convs,
            feat_channels=config.bbox_head.feat_channels,
            norm_cfg=config.bbox_head.get("norm_cfg", None),
            test_cfg=config.test_cfg
        )

    def create_backbone(self, config) -> ResNet:
        """Create backbone and init it."""
        backbone = ResNet(
            depth=config.backbone.depth,
            num_stages=config.backbone.num_stages,
            strides=(1, 2, 2, 2),
            dilations=(1, 1, 1, 1),
            out_indices=config.backbone.out_indices,
            frozen_stages=config.backbone.frozen_stages,
            norm_eval=config.backbone.norm_eval
        )
        new_param = {}
        param_dict = ms.load_checkpoint(config.backbone.pretrained)

        for key, value in param_dict.items():
            if key.startswith('bbox_head.'):
                key = key.replace('gn.weight', 'norm.gamma')\
                   .replace('gn.bias', 'norm.beta')\
                   .replace('fovea_cls', 'conv_cls')\
                   .replace('fovea_reg', 'conv_reg')
            if key.startswith('res_layers.'):
                new_param[key.replace("res_layers.", "0.")] = value
            else:
                new_param[key] = value

        ms.load_param_into_net(backbone, new_param)
        return backbone

    def construct(self, img_data, img_metas, gt_bboxes, gt_labels, gt_valids):
        """
        construct the Foveabox Network.

        Args:
            img_data: input image data.
            img_metas: meta label of img.
            gt_bboxes (Tensor): get the value of bboxes.
            gt_labels (Tensor): get the value of labels.
            gt_valids (Tensor): get the valid part of bboxes.

        Returns:
            Tuple,tuple of output tensor (losses if training else predictions).
        """
        x = self.backbone(img_data)
        x = self.neck(x)
        cls_scores, bbox_preds = self.bbox_head(x)
        if self.training:
            losses = self.bbox_head.loss(
                cls_scores, bbox_preds, gt_bboxes, gt_labels,
                img_metas, gt_bboxes_ignore=gt_valids
            )
            return losses
        return cls_scores, bbox_preds

    def set_train(self, mode=True):
        """Change training mode."""
        super().set_train(mode=mode)
        self.backbone.set_train(mode=mode)


class FoveaboxInfer(nn.Cell):
    """Foveabox wrapper for inference."""

    def __init__(self, net):
        super().__init__()
        self.net = net
        self.net.set_train(False)

    def construct(self, img_data, img_metas=None):
        """Make predictions."""
        cls_scores, bbox_preds = self.net(img_data, img_metas,
                                          None, None, None)

        return bbox_preds, cls_scores

    def __call__(self, *args, **kwargs):
        return self.construct(*args, **kwargs)

    def set_train(self, mode: bool):
        self.net.set_train(mode)
