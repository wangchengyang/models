# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
# This file has been derived from the
# https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
import mindspore as ms
from mindspore import ops
import mindspore.nn as nn
from mindspore.ops import functional as F
from mindspore.ops import operations as P

from .anchor_generator import AnchorGenerator
from .base_dense_head import BaseDenseHead
from .center_region_assigner import CenterRegionAssigner
from .focal_loss import SigmoidFocalClassificationLoss
from .iou_loss import IoULoss
from .tblr_bbox_coder import TBLRBBoxCoder


def images_to_levels(target, num_levels):
    """Convert targets by image to targets by feature level.

    [target_img0, target_img1] -> [target_level0, target_level1, ...]
    """
    target = ops.stack(target, axis=0)
    level_targets = []
    start = ms.Tensor(0).astype(ms.int32)
    one = ms.Tensor(1., ms.int32)
    for n in num_levels:
        end = start + n
        level_targets.append(ops.gather(target,
                                        ops.range(start, end, one),
                                        axis=1))
        start = end
    return level_targets


class ConvWrapper(nn.Cell):
    def __init__(self, conv, act):
        super(ConvWrapper, self).__init__()
        self.conv = conv
        self.act = act

    def construct(self, *inputs, **kwargs):
        return self.act(self.conv(*inputs, **kwargs))


def get_classification_model(in_channel, act, feature_size=256):
    conv1 = ConvWrapper(nn.Conv2d(in_channel, feature_size, kernel_size=3,
                                  pad_mode='same', has_bias=True), act)
    conv2 = ConvWrapper(nn.Conv2d(feature_size, feature_size, kernel_size=3,
                                  pad_mode='same', has_bias=True), act)
    conv3 = ConvWrapper(nn.Conv2d(feature_size, feature_size, kernel_size=3,
                                  pad_mode='same', has_bias=True), act)
    conv4 = ConvWrapper(nn.Conv2d(feature_size, feature_size, kernel_size=3,
                                  pad_mode='same', has_bias=True), act)
    return nn.SequentialCell([conv1, conv2, conv3, conv4])


def get_regression_model(in_channel, act, feature_size=256):
    conv1 = ConvWrapper(nn.Conv2d(in_channel, feature_size, kernel_size=3,
                                  pad_mode='same', has_bias=True), act)
    conv2 = ConvWrapper(nn.Conv2d(feature_size, feature_size, kernel_size=3,
                                  pad_mode='same', has_bias=True), act)
    conv3 = ConvWrapper(nn.Conv2d(feature_size, feature_size, kernel_size=3,
                                  pad_mode='same', has_bias=True), act)
    conv4 = ConvWrapper(nn.Conv2d(feature_size, feature_size, kernel_size=3,
                                  pad_mode='same', has_bias=True), act)
    return nn.SequentialCell([conv1, conv2, conv3, conv4])


class FlattenConcat(nn.Cell):
    """
    Concatenate predictions into a single tensor.

    Args:
        config (dict): The default config of retinanet.

    Returns:
        ms.Tensor, flatten predictions.
    """
    def __init__(self):
        super(FlattenConcat, self).__init__()
        self.num_retinanet_boxes = 67995
        self.concat = P.Concat(axis=1)
        self.transpose = P.Transpose()

    def construct(self, inputs):
        output = ()
        batch_size = F.shape(inputs[0])[0]
        for x in inputs:
            x = self.transpose(x, (0, 2, 3, 1))
            output += (F.reshape(x, (batch_size, -1)),)
        res = self.concat(output)
        return F.reshape(res, (batch_size, self.num_retinanet_boxes // 9, -1))


class FSAFHead(BaseDenseHead):
    """
    Multibox conv layers. Each multibox layer contains class conf scores and localization predictions.

    Args:
        config (dict): The default config of retinanet.

    Returns:
        ms.Tensor, localization predictions.
        ms.Tensor, class conf scores.
    """
    def __init__(self, config, train_cfg, test_cfg):
        super(FSAFHead, self).__init__()

        out_channels = [256, 256, 256, 256, 256]
        self.num_classes = config.num_classes
        self.act = nn.ReLU()
        self.reg_convs = get_regression_model(in_channel=out_channels[0],
                                              act=self.act)
        self.cls_convs = get_classification_model(in_channel=out_channels[0],
                                                  act=self.act)
        self.flatten_concat = FlattenConcat()
        feat_channels = 256
        cls_out_channels = config.num_classes
        self.retina_cls = nn.Conv2d(
            feat_channels,
            cls_out_channels,
            3,
            pad_mode='pad',
            padding=1,
            has_bias=True
        )
        self.retina_reg = nn.Conv2d(
            feat_channels,
            4,  # num_base_priors * 4,
            3,
            pad_mode='pad',
            padding=1,
            has_bias=True
        )

        self.anchor_generator = AnchorGenerator(
            strides=config.anchor_generator.strides,
            ratios=config.anchor_generator.ratios,
            octave_base_scale=config.anchor_generator.octave_base_scale,
            scales_per_octave=config.anchor_generator.scales_per_octave
        )

        self.assigner = CenterRegionAssigner(
            pos_scale=train_cfg.assigner.pos_scale,
            neg_scale=train_cfg.assigner.neg_scale,
            min_pos_iof=train_cfg.assigner.min_pos_iof
        )

        self.bbox_coder = TBLRBBoxCoder(
            normalizer=config.bbox_coder.normalizer
        )
        self.decode = self.bbox_coder.decode
        self.test_cfg = test_cfg
        self.train_cfg = train_cfg
        self.cls_out_channels = config.num_classes
        self.use_sigmoid_cls = True
        self.reg_decoded_bbox = config.reg_decoded_bbox
        self.sampling = False

        self.loss_cls = SigmoidFocalClassificationLoss(config.loss_cls.gamma,
                                                       config.loss_cls.alpha)
        self.loss_bbox = IoULoss(eps=ms.Tensor(float(config.loss_bbox.eps)),
                                 reduction=config.loss_bbox.reduction,
                                 loss_weight=config.loss_bbox.loss_weight)
        self.score_threshold = None
        self.one = ms.Tensor(1., ms.int32)
        self.zero = ms.Tensor(0., ms.int32)
        self.pos_weight = self.train_cfg.pos_weight
        self.allowed_border = self.train_cfg.allowed_border

    def construct(self, inputs):
        res_cls = []
        res_reg = []
        for cls_feat in inputs:
            cls_feat = self.cls_convs(cls_feat)
            cls_feat = self.retina_cls(cls_feat)
            res_cls.append(cls_feat)
        for reg_feat in inputs:
            reg_feat = self.reg_convs(reg_feat)
            reg_feat = self.retina_reg(reg_feat)
            res_reg.append(reg_feat)
        return res_reg, res_cls

    def loss(self,
             cls_scores,
             bbox_preds,
             gt_valids,
             gt_bboxes,
             gt_labels,
             img_metas,
             gt_bboxes_ignore=None):
        """Compute loss of the head.

        Args:
            cls_scores (list[Tensor]): Box scores for each scale level
                Has shape (N, num_points * num_classes, H, W).
            bbox_preds (list[Tensor]): Box energies / deltas for each scale
                level with shape (N, num_points * 4, H, W).
            gt_bboxes (list[Tensor]): each item are the truth boxes for each
                image in [tl_x, tl_y, br_x, br_y] format.
            gt_labels (list[Tensor]): class indices corresponding to each box
            img_metas (list[dict]): Meta information of each image, e.g.,
                image size, scaling factor, etc.
            gt_bboxes_ignore (None | list[Tensor]): specify which bounding
                boxes can be ignored when computing the loss.

        Returns:
            dict[str, ms.Tensor]: A dictionary of loss components.
        """
        for i in range(len(bbox_preds)):  # loop over fpn level
            # avoid 0 area of the predicted bbox
            bbox_preds[i] = ops.clip_by_value(bbox_preds[i],
                                              clip_value_min=ms.Tensor(1e-4))
        feature_shapes = [featmap.shape[-2:] for featmap in cls_scores]

        assert len(feature_shapes) == self.anchor_generator.num_levels
        batch_size, num_gts_per_batch = gt_valids.shape[:2]

        # List of anchors for each level of FPN (without BS dim)
        anchor_list = self.anchor_generator.grid_priors(feature_shapes)

        # List of valid flags for ech level of FPN
        # (*currently all True)
        valid_flag_list = self.anchor_generator.valid_flags(
            feature_shapes,
            img_metas[0][:2]
        )
        label_channels = self.cls_out_channels if self.use_sigmoid_cls else 1

        cls_reg_targets = self.get_targets(
            anchor_list,
            valid_flag_list,
            gt_bboxes,
            img_metas,
            gt_valids,
            gt_bboxes_ignore_list=gt_bboxes_ignore,
            gt_labels_list=gt_labels,
            label_channels=label_channels)

        if cls_reg_targets is None:
            return None
        (labels_list, label_weights_list, bbox_targets_list,
         bbox_weights_list, num_total_pos, num_total_neg,
         pos_assigned_gt_inds_list) = cls_reg_targets

        num_total_samples = (
            num_total_pos + num_total_neg if self.sampling else num_total_pos)
        # anchor number of multi levels
        num_level_anchors = [anchors.shape[0] for anchors in anchor_list]
        # concat all level anchors and flags to a single tensor
        concat_anchor_list = [ops.concat(anchor_list)] * len(img_metas)
        all_anchor_list = images_to_levels(concat_anchor_list,
                                           num_level_anchors)

        losses_cls, losses_bbox = [], []
        # Iterate pyramid levels
        for i in range(len(cls_scores)):
            loss_cls, loss_bbox = self.loss_single(
                cls_scores[i],
                bbox_preds[i],
                all_anchor_list[i],
                labels_list[i],
                label_weights_list[i],
                bbox_targets_list[i],
                bbox_weights_list[i],
                num_total_samples=num_total_samples)
            losses_cls.append(loss_cls)
            losses_bbox.append(loss_bbox)
        # `pos_assigned_gt_inds_list` (length: fpn_levels) stores the assigned
        # gt index of each anchor bbox in each fpn level.
        num_gts = ops.count_nonzero(gt_valids.astype(ms.int32),
                                    axis=1).squeeze(1)
        cum_num_gts = ops.cumsum(num_gts, 0)  # length of batch_size

        new_pos_assigned_gt_inds_list = []
        new_labels_list = []
        for i in range(len(bbox_preds)):
            assign = pos_assigned_gt_inds_list[i]
            # loop over fpn levels
            for j in range(1, batch_size):
                # loop over batch size
                # Convert gt indices in each img to those in the batch
                upd = ops.zeros_like(assign[j])
                upd = ops.masked_fill(upd, assign[j] >= 0,
                                      cum_num_gts[j - 1].astype(ms.int64))
                indx = ops.masked_fill(upd, assign[j] >= 0, j)

                assign = ops.tensor_scatter_elements(assign,
                                                     indx.expand_dims(0),
                                                     upd.expand_dims(0),
                                                     reduction='add')

            new_pos_assigned_gt_inds_list.append(assign.flatten())
            new_labels_list.append(labels_list[i].flatten())
        pos_assigned_gt_inds_list = new_pos_assigned_gt_inds_list
        labels_list = new_labels_list

        # The unique label index of each gt in the batch
        label_sequence = ops.range(
            self.zero,
            ms.Tensor(num_gts_per_batch * batch_size / 2, ms.int32),
            self.one
        )
        # Collect the average loss of each gt in each level
        loss_levels = []
        for i in range(len(losses_cls)):
            loss_levels.append(
                self.collect_loss_level_single(
                    losses_cls[i],
                    losses_bbox[i],
                    pos_assigned_gt_inds_list[i],
                    labels_seq=label_sequence
                )
            )
        # Shape: (fpn_levels, num_gts). Loss of each gt at each fpn level
        loss_levels = ops.stack(loss_levels, axis=0)
        # Locate the best fpn level for loss back-propagation
        argmin, _ = ops.min(loss_levels, axis=0)

        # Reweight the loss of each (anchor, label) pair, so that only those
        #  at the best gt level are back-propagated.
        new_losses_cls, new_losses_bbox, new_pos_inds = [], [], []
        # Iterate pyramid levels
        for i in range(len(losses_cls)):
            loss_cls, loss_bbox, pos_ind = self.reweight_loss_single(
                losses_cls[i],
                losses_bbox[i],
                pos_assigned_gt_inds_list[i],
                labels_list[i],
                i,
                min_levels=argmin
            )
            new_losses_cls.append(loss_cls)
            new_losses_bbox.append(loss_bbox)
            new_pos_inds.append(pos_ind)
        losses_cls = new_losses_cls
        losses_bbox = new_losses_bbox
        pos_inds = new_pos_inds
        num_pos = ops.concat(pos_inds, 0).sum().astype(ms.float32)

        if num_pos == 0:  # No gt
            avg_factor = num_pos + num_total_neg
        else:
            avg_factor = num_pos
        avg_factor = ops.stop_gradient(avg_factor)
        new_losses_cls, new_losses_bbox = [], []
        for i in range(len(losses_cls)):
            new_losses_cls.append((losses_cls[i] / avg_factor).expand_dims(0))
            new_losses_bbox.append(
                (losses_bbox[i] / avg_factor).expand_dims(0))

        return new_losses_cls, new_losses_bbox

    def loss_single(self, cls_score, bbox_pred, anchors, labels, label_weights,
                    bbox_targets, bbox_weights, num_total_samples):
        """Compute loss of a single scale level.

        Args:
            cls_score (Tensor): Box scores for each scale level
                Has shape (N, num_anchors * num_classes, H, W).
            bbox_pred (Tensor): Box energies / deltas for each scale
                level with shape (N, num_anchors * 4, H, W).
            anchors (Tensor): Box reference for each scale level with shape
                (N, num_total_anchors, 4).
            labels (Tensor): Labels of each anchors with shape
                (N, num_total_anchors).
            label_weights (Tensor): Label weights of each anchor with shape
                (N, num_total_anchors)
            bbox_targets (Tensor): BBox regression targets of each anchor
                weight shape (N, num_total_anchors, 4).
            bbox_weights (Tensor): BBox regression loss weights of each anchor
                with shape (N, num_total_anchors, 4).
            num_total_samples (int): If sampling, num total samples equal to
                the number of total anchors; Otherwise, it is the number of
                positive anchors.

        Returns:
            dict[str, ms.Tensor]: A dictionary of loss components.
        """
        # classification loss
        labels = labels.reshape(-1)
        cls_score = (cls_score.transpose((0, 2, 3, 1))
                     .reshape(-1, self.cls_out_channels))
        loss_cls = self.loss_cls(cls_score, labels)
        label_weights = label_weights.reshape(loss_cls.shape)
        loss_cls = loss_cls * label_weights

        # regression loss
        bbox_targets = bbox_targets.reshape(-1, 4)
        bbox_weights = bbox_weights.reshape(-1, 4)
        bbox_pred = bbox_pred.transpose((0, 2, 3, 1)).reshape(-1, 4)
        if self.reg_decoded_bbox:
            # When the regression loss (e.g. `IouLoss`, `GIouLoss`)
            # is applied directly on the decoded bounding boxes, it
            # decodes the already encoded coordinates to absolute format.
            anchors = anchors.reshape(-1, 4)
            bbox_pred = self.decode(anchors, bbox_pred)

        loss_bbox = self.loss_bbox(
            bbox_pred,
            bbox_targets,
            bbox_weights,
            avg_factor=num_total_samples)
        return loss_cls, loss_bbox

    def get_targets(self,
                    anchor_list,
                    valid_flag_list,
                    gt_bboxes_list,
                    img_metas,
                    gt_valids,
                    gt_bboxes_ignore_list=None,
                    gt_labels_list=None,
                    label_channels=1):
        """Compute regression and classification targets for anchors in
        multiple images.

        Args:
            anchor_list (list[list[Tensor]]): Multi level anchors of each
                image. The outer list indicates images, and the inner list
                corresponds to feature levels of the image. Each element of
                the inner list is a tensor of shape (num_anchors, 4).
            valid_flag_list (list[list[Tensor]]): Multi level valid flags of
                each image. The outer list indicates images, and the inner list
                corresponds to feature levels of the image. Each element of
                the inner list is a tensor of shape (num_anchors, )
            gt_bboxes_list (list[Tensor]): Ground truth bboxes of each image.
            img_metas (list[dict]): Meta info of each image.
            gt_bboxes_ignore_list (list[Tensor]): Ground truth bboxes to be
                ignored.
            gt_labels_list (list[Tensor]): Ground truth labels of each box.
            label_channels (int): Channel of label.
            unmap_outputs (bool): Whether to map outputs back to the original
                set of anchors.

        Returns:
            tuple: Usually returns a tuple containing learning targets.

                - labels_list (list[Tensor]): Labels of each level.
                - label_weights_list (list[Tensor]): Label weights of each
                  level.
                - bbox_targets_list (list[Tensor]): BBox targets of each level.
                - bbox_weights_list (list[Tensor]): BBox weights of each level.
                - num_total_pos (int): Number of positive samples in all
                  images.
                - num_total_neg (int): Number of negative samples in all
                  images.

            additional_returns: This function enables user-defined returns from
                `self._get_targets_single`. These returns are currently refined
                to properties at each feature map (i.e. having HxW dimension).
                The results will be concatenated after the end
        """
        num_imgs = len(img_metas)
        # Number of anchors on every level
        num_level_anchors = [anchors.shape[0] for anchors in anchor_list]
        # concat all level anchors to a single tensor
        concat_anchor_list = []
        concat_valid_flag_list = []

        # For each image in batch concat all anchors
        for i in range(num_imgs):
            # assert len(anchor_list[i]) == len(valid_flag_list[i])
            concat_anchor_list.append(ops.concat(anchor_list))
            concat_valid_flag_list.append(ops.concat(valid_flag_list))

        # compute targets for each image
        if gt_bboxes_ignore_list is None:
            gt_bboxes_ignore_list = [None for _ in range(num_imgs)]
        if gt_labels_list is None:
            raise RuntimeError('Unexpected Nones')

        all_labels = []
        all_label_weights = []
        all_bbox_targets = []
        all_bbox_weights = []
        gt_inds = []
        pos_gt_inds = []
        # Iterate batch size
        for i in range(num_imgs):
            result = self._get_targets_single(
                concat_anchor_list[i],
                gt_bboxes_list[i],
                gt_bboxes_ignore_list[i],
                gt_labels_list[i],
                gt_valids[i],
                label_channels=label_channels
            )
            (all_label, all_label_weight, all_bbox_target, all_bbox_weight,
             gt_ind, pos_gt_ind) = result[:7]

            all_labels.append(all_label)
            all_label_weights.append(all_label_weight)
            all_bbox_targets.append(all_bbox_target)
            all_bbox_weights.append(all_bbox_weight)
            gt_inds.append(gt_ind)
            pos_gt_inds.append(pos_gt_ind)
        pos_gt_inds = [pos_gt_inds]
        gt_inds = ops.concat(gt_inds)

        num_total_pos = ops.count_nonzero(gt_inds)
        num_total_neg = gt_inds.size - ops.count_nonzero(gt_inds)
        # split targets to a list w.r.t. multiple levels
        labels_list = images_to_levels(all_labels, num_level_anchors)
        label_weights_list = images_to_levels(all_label_weights,
                                              num_level_anchors)
        bbox_targets_list = images_to_levels(all_bbox_targets,
                                             num_level_anchors)
        bbox_weights_list = images_to_levels(all_bbox_weights,
                                             num_level_anchors)
        res = (labels_list, label_weights_list, bbox_targets_list,
               bbox_weights_list, num_total_pos, num_total_neg)
        for i, r in enumerate(pos_gt_inds):  # user-added return values
            pos_gt_inds[i] = images_to_levels(r, num_level_anchors)

        return res + tuple(pos_gt_inds)

    def _get_targets_single(self,
                            flat_anchors,
                            gt_bboxes,
                            gt_bboxes_ignore,
                            gt_labels,
                            gt_valids,
                            label_channels=1):
        """Compute regression and classification targets for anchors in a
        single image.

        Most of the codes are the same with the base class
          :obj: `AnchorHead`, except that it also collects and returns
          the matched gt index in the image (from 0 to num_gt-1). If the
          anchor bbox is not matched to any gt, the corresponding value in
          pos_gt_inds is -1.
        """
        gt_labels = ops.squeeze(gt_labels, 1)
        gt_valids = gt_valids.squeeze(1)

        # Assign gt and sample anchors
        anchors = flat_anchors
        assign_result = self.assigner.assign(anchors,
                                             gt_bboxes,
                                             gt_valids,
                                             gt_bboxes_ignore,
                                             gt_labels)
        gt_valids = gt_valids.nonzero().squeeze(1)
        gt_inds, shadowed_pixel_labels = assign_result[1], assign_result[3]

        # Sampling
        pos_inds = ops.nonzero(gt_inds > 0).squeeze(1)

        num_valid_anchors = anchors.shape[0]
        bbox_targets = ops.zeros_like(anchors)
        bbox_weights = ops.zeros_like(anchors)
        labels = ops.ones((num_valid_anchors,),
                          ms.int32) * self.cls_out_channels
        label_weights = ops.zeros((num_valid_anchors, label_channels),
                                  ms.float32)
        pos_gt_inds = ops.ones((num_valid_anchors,),
                               ms.int64) * -1

        if ops.reduce_sum(gt_inds > 0) > 0:
            pos_assigned_gt_inds = gt_inds[pos_inds] - 1

            # When the regression loss (e.g. `IouLoss`, `GIouLoss`)
            # is applied directly on the decoded bounding boxes, both
            # the predicted boxes and regression targets should be with
            # absolute coordinate format.
            pos_bbox_targets = ops.gather(gt_bboxes[gt_valids],
                                          pos_assigned_gt_inds,
                                          0)

            index = ops.concat([pos_inds.expand_dims(1) for i in range(4)],
                               axis=1)
            bbox_targets = ops.tensor_scatter_elements(
                bbox_targets,
                index,
                pos_bbox_targets,
            )

            bbox_weights = ops.tensor_scatter_elements(
                bbox_weights,
                index,
                ops.ones_like(index).astype(ms.float32))

            # The assigned gt_index for each anchor. (0-based)
            pos_gt_inds = ops.tensor_scatter_elements(
                pos_gt_inds, pos_inds,
                pos_assigned_gt_inds
            )

            labels = ops.tensor_scatter_elements(
                labels.transpose(), pos_inds,
                gt_labels[gt_valids][pos_assigned_gt_inds]
            )
            if self.pos_weight <= 0:
                label_weights = ops.masked_fill(
                    label_weights,
                    ops.tile((gt_inds > 0).reshape((-1, 1)),
                             (1, self.num_classes)),
                    1.0
                )
            else:
                label_weights = ops.masked_fill(label_weights, pos_inds,
                                                self.pos_weight)
        if gt_inds.size != ops.count_nonzero(gt_inds):
            label_weights = ops.masked_fill(
                label_weights,
                ops.tile((gt_inds == 0).reshape((-1, 1)),
                         (1, self.num_classes)),
                1.0
            )

        # shadowed_labels is a tensor composed of tuples
        #  (anchor_inds, class_label) that indicate those anchors lying in the
        #  outer region of a gt or overlapped by another gt with a smaller
        #  area.
        #
        # Therefore, only the shadowed labels are ignored for loss calculation.
        # the key `shadowed_labels` is defined in :obj:`CenterRegionAssigner`
        if (shadowed_pixel_labels is not None
                and shadowed_pixel_labels.sum() > 0):
            label_weights = ops.select(shadowed_pixel_labels,
                                       ops.zeros_like(label_weights),
                                       label_weights)

        return (labels, label_weights, bbox_targets,
                bbox_weights, gt_inds, pos_gt_inds)

    def collect_loss_level_single(self, cls_loss, reg_loss, assigned_gt_inds,
                                  labels_seq):
        """Get the average loss in each FPN level w.r.t. each gt label.

        Args:
            cls_loss (Tensor): Classification loss of each feature map pixel,
              shape (num_anchor, num_class)
            reg_loss (Tensor): Regression loss of each feature map pixel,
              shape (num_anchor, 4)
            assigned_gt_inds (Tensor): It indicates which gt the prior is
              assigned to (0-based, -1: no assignment). shape (num_anchor),
            labels_seq: The rank of labels. shape (num_gt)

        Returns:
            shape: (num_gt), average loss of each gt in this level
        """
        if len(reg_loss.shape) == 2:  # iou loss has shape (num_prior, 4)
            reg_loss = reg_loss.sum(-1)  # sum loss in tblr dims
        if len(cls_loss.shape) == 2:
            cls_loss = cls_loss.sum(-1)  # sum loss in class dims
        loss = cls_loss + reg_loss
        assert loss.shape[0] == assigned_gt_inds.shape[0]
        # Default loss value is 1e6 for a layer where no anchor is positive
        #  to ensure it will not be chosen to back-propagate gradient
        losses_ = ops.ones_like(labels_seq) * 1e6
        for i in range(labels_seq.size):
            match = assigned_gt_inds == i
            if match.sum() > 0:
                losses_[i] = loss[ops.nonzero(match).squeeze(1)].mean()
        return losses_

    def reweight_loss_single(self, cls_loss, reg_loss, assigned_gt_inds,
                             labels, level, min_levels):
        """Reweight loss values at each level.

        Reassign loss values at each level by masking those where the
        pre-calculated loss is too large. Then return the reduced losses.

        Args:
            cls_loss (Tensor): Element-wise classification loss.
              Shape: (num_anchors, num_classes)
            reg_loss (Tensor): Element-wise regression loss.
              Shape: (num_anchors, 4)
            assigned_gt_inds (Tensor): The gt indices that each anchor bbox
              is assigned to. -1 denotes a negative anchor, otherwise it is the
              gt index (0-based). Shape: (num_anchors, ),
            labels (Tensor): Label assigned to anchors. Shape: (num_anchors, ).
            level (int): The current level index in the pyramid
              (0-4 for RetinaNet)
            min_levels (Tensor): The best-matching level for each gt.
              Shape: (num_gts, ),

        Returns:
            tuple:
                - cls_loss: Reduced corrected classification loss. Scalar.
                - reg_loss: Reduced corrected regression loss. Scalar.
                - pos_flags (Tensor): Corrected bool tensor indicating the
                  final positive anchors. Shape: (num_anchors, ).
        """
        num_bboxes, num_gts = cls_loss.shape
        loc_weight = ops.ones_like(reg_loss)
        cls_weight = ops.ones_like(cls_loss)
        pos_flags = assigned_gt_inds >= 0  # positive pixel flag

        if pos_flags.sum() > 0:  # pos pixels exist
            neg_indices = ops.logical_and(
                pos_flags,
                min_levels[assigned_gt_inds] != level
            )
            if neg_indices.sum() > 0:
                pos_flags = ops.masked_fill(pos_flags, neg_indices, 0)
                loc_weight = ops.masked_fill(loc_weight, neg_indices, 0)

                mask = ops.tile(ms.numpy.arange(num_gts),
                                (num_bboxes, 1))
                mask = ops.logical_and(
                    neg_indices.reshape(-1, 1),
                    mask == labels.reshape(-1, 1)
                )
                cls_weight = ops.masked_fill(cls_weight, mask, 0)

        cls_weight = ops.stop_gradient(cls_weight)
        loc_weight = ops.stop_gradient(loc_weight)
        # Weighted loss for both cls and reg loss
        cls_loss = (cls_loss * cls_weight).sum()
        reg_loss = (reg_loss * loc_weight).sum()

        return cls_loss, reg_loss, pos_flags
