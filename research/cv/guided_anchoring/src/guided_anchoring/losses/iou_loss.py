# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the
# https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
import mindspore as ms
import mindspore.nn as nn
import mindspore.numpy as np
import mindspore.ops as ops


class BoundedIoULoss(nn.LossBase):

    def __init__(self, beta=0.2, eps=1e-3, reduction='mean', loss_weight=1.0):
        super().__init__(reduction=reduction)
        self.beta = beta
        self.eps = eps
        self.reduction = reduction
        self.loss_weight = loss_weight

    def construct(self, pred, target, weight=None, avg_factor=None, **kwargs):
        loss = self.loss_weight * self.bounded_iou_loss(
            pred,
            target,
            beta=self.beta,
            eps=self.eps,
            )
        loss = self.get_loss(loss, weight)

        return loss

    def bounded_iou_loss(self, pred, target, beta=0.2, eps=1e-3):
        """
        BIoULoss.

        This is an implementation of paper
        `Improving Object Localization with Fitness NMS and Bounded IoU Loss.
        <https://arxiv.org/abs/1711.00164>`_.

        Args:
        ----
            pred (Tensor): Predicted bboxes.
            target (Tensor): Target bboxes.
            beta (float): beta parameter in smoothl1.
            eps (float): eps to avoid NaN.
        """
        if pred.shape[0] == 0:
            return ms.Tensor(0.0, dtype=ms.float32)
        pred_ctrx = (pred[:, 0] + pred[:, 2]) * 0.5
        pred_ctry = (pred[:, 1] + pred[:, 3]) * 0.5
        pred_w = pred[:, 2] - pred[:, 0]
        pred_h = pred[:, 3] - pred[:, 1]
        target_ctrx = (target[:, 0] + target[:, 2]) * 0.5
        target_ctry = (target[:, 1] + target[:, 3]) * 0.5
        target_w = target[:, 2] - target[:, 0]
        target_h = target[:, 3] - target[:, 1]
        target_ctrx = ops.stop_gradient(target_ctrx)
        target_ctry = ops.stop_gradient(target_ctry)
        target_w = ops.stop_gradient(target_w)
        target_h = ops.stop_gradient(target_h)

        dx = target_ctrx - pred_ctrx
        dy = target_ctry - pred_ctry

        loss_dx = 1 - ops.maximum(
            (target_w - 2 * dx.abs()) /
            (target_w + 2 * dx.abs() + eps), ops.zeros_like(dx))
        loss_dy = 1 - ops.maximum(
            (target_h - 2 * dy.abs()) /
            (target_h + 2 * dy.abs() + eps), ops.zeros_like(dy))
        loss_dw = 1 - ops.minimum(target_w / (pred_w + eps), pred_w /
                                  (target_w + eps))
        loss_dh = 1 - ops.minimum(target_h / (pred_h + eps), pred_h /
                                  (target_h + eps))
        # view(..., -1) does not work for empty tensor
        loss_comb = ops.flatten(
            ops.stack([loss_dx, loss_dy, loss_dw, loss_dh], axis=-1)
        )

        loss = np.where(loss_comb < beta, 0.5 * loss_comb * loss_comb / beta,
                        loss_comb - 0.5 * beta)
        return loss
