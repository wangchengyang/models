# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the
# https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
import mindspore.nn as nn


class RetinaNet(nn.Cell):
    """Implementation of `RetinaNet <https://arxiv.org/abs/1708.02002>`_."""

    def __init__(self, backbone, neck, bbox_head):
        super().__init__()
        self.backbone = backbone
        self.neck = neck
        self.bbox_head = bbox_head

    def extract_feat(self, img):
        """Directly extract features from the backbone+neck."""
        x = self.backbone(img)
        x = self.neck(x)
        return x

    def simple_test(self, img, img_metas, rescale=False):
        """
        Test function without test-time augmentation.

        Args:
        ----
            img (Tensor): Images with shape (N, C, H, W).
            img_metas: List of image information.
            rescale (bool, optional): Whether to rescale the results.
                Defaults to False.

        Returns:
        -------
            list[list[np.ndarray]]: BBox results of each image and classes.
                The outer list corresponds to each image. The inner list
                corresponds to each class.
        """
        feat = self.extract_feat(img)

        results_list = self.bbox_head.simple_test(
            feat, img_metas, rescale=rescale)

        return results_list

    def run_train(self,
                  img,
                  img_metas,
                  gt_bboxes,
                  gt_labels,
                  gt_bboxes_ignore=None):
        """
        Args:
        ----
            img (Tensor): Input images of shape (N, C, H, W).
                Typically, these should be mean centered and std scaled.
            img_metas: A List of image info (image size).
            gt_bboxes (list[Tensor]): Each item are the truth boxes for each
                image in [tl_x, tl_y, br_x, br_y] format.
            gt_labels (list[Tensor]): Class indices corresponding to each box
            gt_bboxes_ignore (None | list[Tensor]): Specify which bounding
                boxes can be ignored when computing the loss.

        Returns:
        -------
            dict[str, Tensor]: A dictionary of loss components.
        """
        x = self.extract_feat(img)
        losses = self.bbox_head.forward_train(
            x,
            img_metas, gt_bboxes,
            gt_labels, gt_bboxes_ignore
        )
        return losses

    def construct(
            self,
            img_data,
            img_metas,
            gt_bboxes=None,
            gt_labels=None,
            gt_valids=None
    ):
        """
        Construct the RetinaNet Network.

        Args:
        ----
            img_data: input image data.
            img_metas: meta label of img.
            gt_bboxes (Tensor): get the value of bboxes.
            gt_labels (Tensor): get the value of labels.
            gt_valids (Tensor): get the valid part of bboxes.

        Returns:
        -------
            Tuple,tuple of output tensor
        """
        if self.training:
            output = self.run_train(
                img_data, img_metas, gt_bboxes, gt_labels,
                gt_bboxes_ignore=gt_valids
            )
        else:
            output = self.simple_test(img_data, img_metas)

        return output
