# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from typing import Union

from .resnet import ResNet
from .resnext import ResNeXt
from ...config import Config


def create_backbone(config: Config) -> Union[ResNet, ResNeXt]:
    if config.type == 'ResNet':
        return ResNet(
            depth=config.depth,
            frozen_stages=config.frozen_stages,
            norm_cfg=config.norm_cfg.cfg_dict,
            norm_eval=config.norm_eval,
            num_stages=config.num_stages,
            out_indices=config.out_indices,
            style=config.style,
            pretrained=config.pretrained,
        )
    if config.type == 'ResNeXt':
        return ResNeXt(
            depth=config.depth,
            groups=config.groups,
            base_width=config.base_width,
            frozen_stages=config.frozen_stages,
            norm_cfg=config.norm_cfg.cfg_dict,
            norm_eval=config.norm_eval,
            num_stages=config.num_stages,
            out_indices=config.out_indices,
            style=config.style,
            pretrained=config.pretrained,
        )
    raise RuntimeError(f'Unknown backbone type: {config.type}')
