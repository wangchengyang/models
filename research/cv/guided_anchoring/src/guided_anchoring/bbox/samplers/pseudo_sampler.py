# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import mindspore as ms
import mindspore.ops as ops

from .sampling_result import SamplingResult


class PseudoSampler:
    """A pseudo sampler that does not do sampling actually."""

    def __init__(self, **kwargs):
        pass

    def _sample_pos(self, **kwargs):
        """Sample positive samples."""
        raise NotImplementedError

    def _sample_neg(self, **kwargs):
        """Sample negative samples."""
        raise NotImplementedError

    def sample(self, assign_result, bboxes, gt_bboxes, *args, **kwargs):
        """
        Directly returns the positive and negative indices  of samples.

        Args:
        ----
            assign_result (:obj:`AssignResult`): Assigned results
            bboxes (Tensor): Bounding boxes
            gt_bboxes (Tensor): Ground truth boxes

        Returns:
        -------
            :obj:`SamplingResult`: sampler results
        """
        pos_inds = (assign_result.gt_inds > 0).nonzero()
        if pos_inds.size > 0:
            pos_inds = ops.unique(ops.squeeze(pos_inds, axis=-1))
        neg_inds = (assign_result.gt_inds == 0).nonzero()
        if neg_inds.size > 0:
            neg_inds = ops.unique(ops.squeeze(neg_inds, axis=-1))

        gt_flags = ops.zeros(bboxes.shape[0], ms.uint8)
        sampling_result = SamplingResult(pos_inds, neg_inds, bboxes, gt_bboxes,
                                         assign_result, gt_flags)
        return sampling_result
