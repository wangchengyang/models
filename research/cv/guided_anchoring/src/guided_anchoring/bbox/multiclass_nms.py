# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import numpy as np


def apply_nms(all_boxes, all_scores, thres, max_boxes):
    """Apply NMS to bboxes."""
    y1 = all_boxes[:, 0]
    x1 = all_boxes[:, 1]
    y2 = all_boxes[:, 2]
    x2 = all_boxes[:, 3]
    areas = (x2 - x1 + 1) * (y2 - y1 + 1)

    order = all_scores.argsort()[::-1]
    keep = []

    while order.size > 0:
        i = order[0]
        keep.append(i)

        if len(keep) >= max_boxes:
            break

        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = w * h

        ovr = inter / (areas[i] + areas[order[1:]] - inter)

        inds = np.where(ovr <= thres)[0]

        order = order[inds + 1]
    return keep


def multiclass_nms(
        bboxes, scores, min_score=0.05, nms_threshold=0.5, max_boxes=100,
        num_classes=80
):
    final_boxes = []
    final_label = []
    final_score = []

    for c in range(0, num_classes):
        class_box_scores = scores[:, c]
        score_mask = class_box_scores > min_score
        class_box_scores = class_box_scores[score_mask]
        class_boxes = bboxes[score_mask]

        if score_mask.any():
            nms_index = apply_nms(class_boxes,
                                  class_box_scores,
                                  nms_threshold,
                                  max_boxes)

            class_boxes = class_boxes[nms_index]
            class_box_scores = class_box_scores[nms_index]
            final_boxes += class_boxes.tolist()
            final_score += class_box_scores.tolist()
            final_label += [c + 1] * len(class_box_scores)
    final_boxes = np.array(final_boxes)
    final_score = np.array(final_score)
    final_label = np.array(final_label)

    return final_boxes, final_score, final_label


def bbox_overlaps(bboxes1,
                  bboxes2,
                  mode='iou',
                  eps=1e-6,
                  use_legacy_coordinate=False):
    """
    Calculate the ious between each bbox of bboxes1 and bboxes2.

    Args:
    ----
        bboxes1 (ndarray): Shape (n, 4)
        bboxes2 (ndarray): Shape (k, 4)
        mode (str): IOU (intersection over union) or IOF (intersection
            over foreground)
        use_legacy_coordinate (bool): Whether to use coordinate system,
            which means width, height should be
            calculated as 'x2 - x1 + 1` and 'y2 - y1 + 1' respectively.
            Default: False.

    Returns:
    -------
        ious (ndarray): Shape (n, k)
    """
    assert mode in ['iou', 'iof']
    if not use_legacy_coordinate:
        extra_length = 0.
    else:
        extra_length = 1.
    bboxes1 = bboxes1.astype(np.float32)
    bboxes2 = bboxes2.astype(np.float32)
    rows = bboxes1.shape[0]
    cols = bboxes2.shape[0]
    ious = np.zeros((rows, cols), dtype=np.float32)
    if rows * cols == 0:
        return ious
    exchange = False
    if bboxes1.shape[0] > bboxes2.shape[0]:
        bboxes1, bboxes2 = bboxes2, bboxes1
        ious = np.zeros((cols, rows), dtype=np.float32)
        exchange = True
    area1 = (bboxes1[:, 2] - bboxes1[:, 0] + extra_length) * (
        bboxes1[:, 3] - bboxes1[:, 1] + extra_length)
    area2 = (bboxes2[:, 2] - bboxes2[:, 0] + extra_length) * (
        bboxes2[:, 3] - bboxes2[:, 1] + extra_length)
    for i in range(bboxes1.shape[0]):
        x_start = np.maximum(bboxes1[i, 0], bboxes2[:, 0])
        y_start = np.maximum(bboxes1[i, 1], bboxes2[:, 1])
        x_end = np.minimum(bboxes1[i, 2], bboxes2[:, 2])
        y_end = np.minimum(bboxes1[i, 3], bboxes2[:, 3])
        overlap = np.maximum(x_end - x_start + extra_length, 0) * np.maximum(
            y_end - y_start + extra_length, 0)
        if mode == 'iou':
            union = area1[i] + area2 - overlap
        else:
            union = area1[i] if not exchange else area2
        union = np.maximum(union, eps)
        ious[i, :] = overlap / union
    if exchange:
        ious = ious.T
    return ious


def fast_nms(multi_bboxes, multi_scores, multi_coeffs=None,
             score_thr=0.05, iou_thr=0.5,
             top_k=100, num_classes=80, max_num=-1):
    """
    Fast NMS in `YOLACT <https://arxiv.org/abs/1904.02689>`_.

    Fast NMS allows already-removed detections to suppress other detections so
    that every instance can be decided to be kept or discarded in parallel,
    which is not possible in traditional NMS. This relaxation allows us to
    implement Fast NMS entirely in standard GPU-accelerated matrix operations.

    Args:
    ----
        multi_bboxes (Tensor): shape (n, #class*4) or (n, 4)
        multi_scores (Tensor): shape (n, #class+1), where the last column
            contains scores of the background class, but this will be ignored.
        multi_coeffs (Tensor): shape (n, #class*coeffs_dim).
        score_thr (float): bbox threshold, bboxes with scores lower than it
            will not be considered.
        iou_thr (float): IoU threshold to be considered as conflicted.
        top_k (int): if there are more than top_k bboxes before NMS,
            only top top_k will be kept.
        max_num (int): if there are more than max_num bboxes after NMS,
            only top max_num will be kept. If -1, keep all the bboxes.
            Default: -1.

    Returns:
    -------
        tuple: (dets, labels, coefficients), tensors of shape (k, 5), (k, 1),
            and (k, coeffs_dim). Dets are boxes with scores.
            Labels are 0-based.
    """
    scores = multi_scores[:, :-1].T  # [#class, n]
    idx = np.argsort(scores, 1)[:, ::-1].copy()
    scores = np.take_along_axis(scores, idx, 1)

    idx = idx[:, :top_k].copy()
    scores = scores[:, :top_k]  # [#class, topk]
    num_classes, num_dets = idx.shape
    boxes = multi_bboxes[np.ravel(idx), :].reshape((num_classes, num_dets, 4))

    iou = np.array([
        bbox_overlaps(cboxes, cboxes)  # [#class, topk, topk]
        for cboxes in boxes  # [#class]
    ])
    iou = np.triu(iou, 1)
    iou_max = iou.max(axis=1)

    # Now just filter out the ones higher than the threshold
    keep = iou_max <= iou_thr

    # Second thresholding introduces 0.2 mAP gain at negligible time cost
    keep *= scores > score_thr

    # Assign each kept detection to its corresponding class
    classes = np.broadcast_to(np.arange(
        num_classes)[:, None], keep.shape)
    classes = classes[keep]

    boxes = boxes[keep]
    scores = scores[keep]

    # Only keep the top max_num highest scores across all classes
    idx = np.argsort(scores, 0)[::-1]
    scores = np.take_along_axis(scores, idx, 0)
    if max_num > 0:
        idx = idx[:max_num]
        scores = scores[:max_num]

    classes = classes[idx]
    boxes = boxes[idx]

    # cls_dets = np.concatenate([boxes, scores[:, None]], axis=1)
    return boxes, scores, classes


def norm_bboxes(boxes, meta):
    """Norm bboxes according original shape."""
    if boxes:
        boxes[..., [0, 2]] = np.clip(
            boxes[..., [0, 2]], a_min=0, a_max=meta[3]
        )
        boxes[..., [1, 3]] = np.clip(
            boxes[..., [1, 3]], a_min=0, a_max=meta[2]
        )

        boxes[..., [0, 2]] /= meta[3]
        boxes[..., [1, 3]] /= meta[2]
        boxes[:, [0, 2]] *= meta[1]
        boxes[:, [1, 3]] *= meta[0]
    return boxes
