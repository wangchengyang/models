# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/rover-xingyu/Ha-NeRF
# ============================================================================
"""Evaluation using the test split of passed dataset."""

import os
import argparse
import json
import csv
from collections import defaultdict
from pathlib import Path

import imageio
import numpy as np
from PIL import Image
from tqdm import tqdm

import mindspore as ms
from mindspore import ops
from mindspore.dataset import vision, GeneratorDataset
from mindspore.train.serialization import (
    load_checkpoint, load_param_into_net
)

from src.models.nerf_system import NeRFSystem
from src.data.phototourism_mask_grid_sample import PhotoTourismDataset
from src.tools.metrics import get_psnr

DEFAULT_DS_CONFIG = Path('src') / 'configs' / 'phototourism_ds_config.json'
DEFAULT_MODEL_CONFIG = Path('src') / 'configs' / 'nerf_config.json'


def get_opts():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--data-path', type=str, required=True,
                        help='Dataset directory.')
    parser.add_argument('--data-config', type=str,
                        default=DEFAULT_DS_CONFIG,
                        help='Path to the dataset config.')
    parser.add_argument('--train-config', type=str,
                        required=True,
                        help='Path to the train config.')
    parser.add_argument('--model-config', type=str,
                        default=DEFAULT_MODEL_CONFIG,
                        help='Volume rendering model config.')
    parser.add_argument('--model-ckpt', type=str, required=True,
                        help='Model checkpoints.')
    parser.add_argument('--out-path', type=str, required=True,
                        help='Path for output data saving:'
                             'predicted image, configs, ')
    parser.add_argument('--mode', choices=['graph', 'pynative'],
                        default='graph',
                        help='Model representation mode. '
                             'Pynative for debugging.')
    return parser.parse_args()


def batched_inference(model, ray, a_embedded, chunk):
    """Do batched inference on rays using chunk."""
    rays_length = ray.shape[0]
    res = defaultdict(list)

    for ind in range(0, rays_length, chunk):
        rendered_ray_chunks = \
            model(ray[ind:ind + chunk], a_embedded, test_time=True)

        for k, v in rendered_ray_chunks.items():
            res[k] += [v]

    for k, v in res.items():
        res[k] = ops.cat(v, 0)
    return res


def main():
    args = get_opts()

    mode = ms.GRAPH_MODE if args.mode == 'graph' else ms.PYNATIVE_MODE
    ms.set_context(mode=mode,
                   device_target='GPU')
    kwargs = {'root_dir': args.data_path,
              'split': "test_test"}
    with open(args.train_config, 'r') as f:
        train_cfg = json.load(f)
    with open(args.data_config, 'r') as f:
        data_cfg = json.load(f)
    with open(args.model_config, 'r') as f:
        model_cfg = json.load(f)
    kwargs['img_downscale'] = data_cfg["img_downscale"]
    kwargs['use_cache'] = data_cfg["use_cache"]
    dataset = PhotoTourismDataset(**kwargs)

    dataset = GeneratorDataset(dataset,
                               column_names=['rays', 'ts', 'rgbs', 'whole_img',
                                             'img_wh', 'uv_sample'],
                               shuffle=False,
                               num_parallel_workers=1)

    nerf = NeRFSystem(train_config=train_cfg,
                      nerf_config=model_cfg)

    param_dict = load_checkpoint(args.model_ckpt)
    load_param_into_net(nerf, param_dict)
    enc_a = nerf.enc_a

    dir_name = args.out_path
    os.makedirs(dir_name, exist_ok=True)

    toTensor = vision.ToTensor()
    imgs = []
    metrics = {}

    for i, (rays, _, rgbs, whole_img, img_wh, _) in \
            tqdm(enumerate(dataset)):

        a_embedded_from_img = None
        if train_cfg["encode_a"]:
            a_embedded_from_img = enc_a(whole_img)
        results = batched_inference(nerf.network,
                                    rays,
                                    a_embedded_from_img,
                                    train_cfg["batch_size"])

        w, h = img_wh
        w, h = int(w.asnumpy().item()), int(h.asnumpy().item())

        results['rgb_fine'] = results['rgb_fine'].astype(ms.float32)

        img_pred = np.clip(results['rgb_fine'].view(h, w, 3).asnumpy(), 0, 1)
        img_pred_ = (img_pred * 255).astype(np.uint8)
        imgs += [img_pred_]
        imageio.imwrite(os.path.join(dir_name, f'{i:03d}.png'), img_pred_)

        img_gt = rgbs.view(h, w, 3).asnumpy()
        pred = Image.fromarray(img_pred_).convert('RGB')
        pred = toTensor(pred)
        psnr = get_psnr(img_gt[:, w // 2:, :],
                        pred.transpose(1, 2, 0)[:, w // 2:, :])
        metrics[i] = {'img_idx': i, 'psnr': psnr}

    # Write to csv.
    csv_metrics = f"{args.out_path}/metrics.csv"
    with open(csv_metrics, 'w', newline='') as csvfile:
        fieldnames = ['img_idx', 'psnr']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows([v for v in metrics.values()])
        mean_metric_writer = csv.writer(csvfile)
        psnr = np.mean(np.array([v['psnr'] for v in metrics.values()]))
        mean_metric_writer.writerow([f'Mean_PSNR: {psnr}'])


if __name__ == "__main__":
    main()
