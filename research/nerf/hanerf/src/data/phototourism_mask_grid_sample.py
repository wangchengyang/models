# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/rover-xingyu/Ha-NeRF
# ============================================================================
"""Phototourism data processing."""

import os
import glob
import pickle
from math import sqrt, exp
import numpy as np
import pandas as pd
from PIL import Image

import mindspore as ms
from mindspore import ops
from mindspore.dataset import vision

from .colmap_utils import \
    read_cameras_binary, read_images_binary, read_points3d_binary
from ..tools.rays import get_rays


class PhotoTourismDataset:
    def __init__(self,
                 root_dir,
                 split='train',
                 img_downscale=1,
                 val_num=1,
                 use_cache=False,
                 batch_size=1024,
                 scale_anneal=-1,
                 min_scale=0.25):
        """
        img_downscale: how much scale to downsample the training images.
                       The original image sizes are around 500~100
                       so value of 1 or 2 are recommended.
                       ATTENTION! Value of 1 will consume large CPU memory,
                       about 40G for brandenburg gate.
        val_num: number of val images
                 (used for multigpu, validate same image for all gpus)
        use_cache: during data preparation, use precomputed rays
                   (useful to accelerate data loading, especially for multigpu)
        """
        self.step = 0
        self.root_dir = root_dir
        self.split = split
        # image can only be down sampled, please set img_downscale>=1!
        assert img_downscale >= 1
        self.img_downscale = img_downscale

        if ('hagia_sophia_interior' in self.root_dir) \
                or ('taj_mahal' in self.root_dir):
            self.img_downscale_appearance = 4
        else:
            self.img_downscale_appearance = 8

        if split == 'val':  # image downscale=1 will cause OOM in val mode
            self.img_downscale = max(2, self.img_downscale)
        self.val_num = max(1, val_num)  # at least 1
        self.use_cache = use_cache
        self.define_transforms()

        self.read_meta()
        self.white_back = False

        # no effect if scale_anneal<0, else the minimum scale
        # decreases exponentially until converge to min_scale
        self.scale_anneal = scale_anneal
        self.min_scale = min_scale

        self.batch_size = batch_size

    def define_train_data(self):
        if self.use_cache:
            self.all_rays = np.load(
                os.path.join(self.root_dir,
                             f'cache/rays{self.img_downscale}.npy')
            )
            self.all_rgbs = np.load(
                os.path.join(self.root_dir,
                             f'cache/rgbs{self.img_downscale}.npy')
            )

            with open(os.path.join(self.root_dir,
                                   f'cache/all_imgs{8}.pkl'), 'rb') as f:
                self.all_imgs = pickle.load(f)
            self.all_imgs_wh = np.load(
                os.path.join(self.root_dir,
                             f'cache/all_imgs_wh{self.img_downscale}.npy')
            )
        else:
            self.all_rays = []
            self.all_rgbs = []
            self.all_imgs = []
            self.all_imgs_wh = []
            for id_ in self.img_ids_train:
                c2w = np.array(self.poses_dict[id_])

                img = Image.open(
                    os.path.join(self.root_dir, 'dense/images',
                                 self.image_paths[id_])
                ).convert('RGB')
                img_w, img_h = img.size
                if self.img_downscale > 1:
                    img_w = img_w // self.img_downscale
                    img_h = img_h // self.img_downscale
                img_rs = img.resize((img_w, img_h), Image.LANCZOS)
                img_rs = self.transform(img_rs)

                img_8 = \
                    img.resize((img_w // self.img_downscale_appearance,
                                img_h // self.img_downscale_appearance),
                               Image.LANCZOS)
                img_8 = self.normalize(self.transform((img_8)))
                self.all_imgs += [img_8]
                self.all_imgs_wh += \
                    [np.expand_dims(np.array([img_w, img_h]), 0)]
                img_rs = \
                    img_rs.reshape(3, -1).transpose(1, 0)  # (h*w, 3) RGB
                self.all_rgbs += [img_rs]

                rays_o, _, rays_d = \
                    get_rays(img_h, img_w, self.Ks[id_], c2w)
                rays_t = id_ * np.ones((len(rays_o), 1))
                self.all_rays += [np.concatenate([
                    rays_o, rays_d,
                    self.nears[id_] * np.ones_like(rays_o[:, :1]),
                    self.fars[id_] * np.ones_like(rays_o[:, :1]),
                    rays_t], 1)]  # (h*w, 8)

            self.all_rays = \
                np.concatenate(self.all_rays, 0)  # ((N_images-1)*h*w, 8)
            self.all_rgbs = \
                np.concatenate(self.all_rgbs, 0)  # ((N_images-1)*h*w, 3)
            self.all_imgs_wh = \
                np.concatenate(self.all_imgs_wh,
                               0)  # ((N_images-1)*h*w, 3)

    def correct_scale(self, w2c_mats):
        if self.use_cache:
            self.xyz_world = np.load(os.path.join(self.root_dir,
                                                  'cache/xyz_world.npy'))
            with open(os.path.join(self.root_dir,
                                   'cache/nears.pkl'), 'rb') as f:
                self.nears = pickle.load(f)
            with open(os.path.join(self.root_dir,
                                   'cache/fars.pkl'), 'rb') as f:
                self.fars = pickle.load(f)
        else:
            pts3d = read_points3d_binary(
                os.path.join(self.root_dir, 'dense/sparse/points3D.bin')
            )
            self.xyz_world = np.array([pts3d[p_id].xyz for p_id in pts3d])
            xyz_world_h = \
                np.concatenate([self.xyz_world,
                                np.ones((len(self.xyz_world), 1))], -1)
            # Compute near and far bounds for each image individually
            self.nears, self.fars = {}, {}  # {id_: distance}
            for i, id_ in enumerate(self.img_ids):
                # xyz in the ith cam coordinate
                xyz_cam_i = (xyz_world_h @ w2c_mats[i].T)[:, :3]
                # filter out points that lie behind the cam
                xyz_cam_i = xyz_cam_i[xyz_cam_i[:, 2] > 0]
                self.nears[id_] = np.percentile(xyz_cam_i[:, 2], 0.1)
                self.fars[id_] = np.percentile(xyz_cam_i[:, 2], 99.9)

            max_far = np.fromiter(self.fars.values(), np.float32).max()
            scale_factor = max_far / 5  # so that the max far is scaled to 5
            self.poses[..., 3] /= scale_factor
            for k in self.nears:
                self.nears[k] /= scale_factor
            for k in self.fars:
                self.fars[k] /= scale_factor
            self.xyz_world /= scale_factor

    def read_meta(self):
        # read all files in the tsv first (split to train and test later)
        tsv = glob.glob(os.path.join(self.root_dir, '*.tsv'))[0]
        self.scene_name = os.path.basename(tsv)[:-4]
        self.files = pd.read_csv(tsv, sep='\t')
        # remove data without id
        self.files = self.files[~self.files['id'].isnull()]
        self.files.reset_index(inplace=True, drop=True)

        # Step 1. load image paths
        # Attention! The 'id' column in the tsv is BROKEN, don't use it!!!
        # Instead, read the id from images.bin using image file name!
        if self.use_cache:
            with open(os.path.join(self.root_dir,
                                   'cache/img_ids.pkl'), 'rb') as f:
                self.img_ids = pickle.load(f)
            with open(os.path.join(self.root_dir,
                                   'cache/image_paths.pkl'), 'rb') as f:
                self.image_paths = pickle.load(f)
        else:
            imdata = \
                read_images_binary(os.path.join(self.root_dir,
                                                'dense/sparse/images.bin'))
            img_path_to_id = {}
            for v in imdata.values():
                img_path_to_id[v.name] = v.id
            self.img_ids = []
            self.image_paths = {}  # {id: filename}
            for filename in list(self.files['filename']):
                if filename in img_path_to_id:
                    id_ = img_path_to_id[filename]
                    self.image_paths[id_] = filename
                    self.img_ids += [id_]
        # Step 2: read and rescale camera intrinsics
        if self.use_cache:
            with open(os.path.join(self.root_dir,
                                   f'cache/Ks{self.img_downscale}.pkl'),
                      'rb') as f:
                self.Ks = pickle.load(f)
        else:
            self.Ks = {}  # {id: K}
            camdata = read_cameras_binary(
                os.path.join(self.root_dir, 'dense/sparse/cameras.bin')
            )
            for id_ in self.img_ids:
                K = np.zeros((3, 3), dtype=np.float32)
                cam = camdata[id_]
                img_w, img_h = int(cam.params[2] * 2), int(cam.params[3] * 2)
                img_w_, img_h_ = \
                    img_w // self.img_downscale, img_h // self.img_downscale
                K[0, 0] = cam.params[0] * img_w_ / img_w  # fx
                K[1, 1] = cam.params[1] * img_h_ / img_h  # fy
                K[0, 2] = cam.params[2] * img_w_ / img_w  # cx
                K[1, 2] = cam.params[3] * img_h_ / img_h  # cy
                K[2, 2] = 1
                self.Ks[id_] = K
        # Step 3: read c2w poses (of the images in tsv file only)
        # and correct the order
        if self.use_cache:
            w2c_mats = None
            self.poses = np.load(os.path.join(self.root_dir,
                                              'cache/poses.npy'))
        else:
            w2c_mats = []
            bottom = np.array([0, 0, 0, 1.]).reshape(1, 4)
            for id_ in self.img_ids:
                im = imdata[id_]
                R = im.qvec2rotmat()
                t = im.tvec.reshape(3, 1)
                w2c_mats += [np.concatenate([np.concatenate([R, t], 1),
                                             bottom], 0)]
            w2c_mats = np.stack(w2c_mats, 0)  # (N_images, 4, 4)
            self.poses = np.linalg.inv(w2c_mats)[:, :3]  # (N_images, 3, 4)
            # Original poses has rotation in form
            # "right down front", change to "right up back"
            self.poses[..., 1:3] *= -1
        # Step 4: correct scale
        self.correct_scale(w2c_mats)
        self.poses_dict = \
            {id_: self.poses[i] for i, id_ in enumerate(self.img_ids)}
        # Step 5. split the img_ids
        # (the number of images is verified to match that in the paper)
        self.img_ids_train = [id_ for i, id_ in enumerate(self.img_ids)
                              if self.files.loc[i, 'split'] == 'train']
        self.img_ids_test = [id_ for i, id_ in enumerate(self.img_ids)
                             if self.files.loc[i, 'split'] == 'test']
        self.img_names_test = [self.files.loc[i, 'filename']
                               for i, id_ in enumerate(self.img_ids)
                               if self.files.loc[i, 'split'] == 'test']
        self.N_images_train = len(self.img_ids_train)
        self.N_images_test = len(self.img_ids_test)
        if self.split == 'train':  # create buffer of all rays and rgb data
            self.define_train_data()
        # use the first image as val image (also in train)
        elif self.split in ['val', 'test_train']:
            self.val_id = self.img_ids_train[0]

    def define_transforms(self):
        self.transform = vision.ToTensor()
        self.normalize = vision.Normalize(mean=[0.5, 0.5, 0.5],
                                          std=[0.5, 0.5, 0.5],
                                          is_hwc=False)

    def __len__(self):
        if self.split == 'train':
            self.iterations = len(self.all_rays) // self.batch_size
            return self.iterations
        if self.split == 'test_train':
            return self.N_images_train
        if self.split == 'val':
            return self.val_num
        if self.split == 'test_test':
            return self.N_images_test
        return self.val_num

    def __getitem__(self, idx):
        if self.split == 'train':  # use data in the buffers
            current_epoch = self.step // self.iterations
            np.random.seed(current_epoch * self.iterations + idx)
            sample_ts = np.random.randint(0, len(self.all_imgs))
            img_w, img_h = self.all_imgs_wh[sample_ts]
            img = self.all_imgs[sample_ts]
            w_samples, h_samples = \
                ops.meshgrid(ops.linspace(0., float(1 - 1 / img_w),
                                          int(sqrt(self.batch_size))),
                             ops.linspace(0., float(1 - 1 / img_h),
                                          int(sqrt(self.batch_size))),
                             indexing='ij')
            if self.scale_anneal > 0:
                min_scale_cur = min(max(self.min_scale, 1. * exp(
                    -(current_epoch * self.iterations + idx)
                    * self.scale_anneal)), 0.9)
            else:
                min_scale_cur = self.min_scale
            scale = ops.uniform((1,),
                                ms.Tensor(min_scale_cur),
                                ms.Tensor(1.))
            h_offset = ops.uniform((1,), ms.Tensor(0.),
                                   ms.Tensor(1 - scale.item()) *
                                   (1 - 1 / img_h))
            w_offset = ops.uniform((1,), ms.Tensor(0.),
                                   ms.Tensor(1 - scale.item()) *
                                   (1 - 1 / img_w))
            h_sb = h_samples * scale + h_offset
            w_sb = w_samples * scale + w_offset
            h = (h_sb * img_h).floor()
            w = (w_sb * img_w).floor()
            img_sample_points = (w + h * img_w).permute(1, 0).view(-1)
            uv_sample = \
                ops.cat(
                    (h_sb.permute(1, 0).view(-1, 1),
                     w_sb.permute(1, 0).view(-1, 1)), -1)
            rgb_sample_points = \
                (img_sample_points + (
                    self.all_imgs_wh[:sample_ts, 0] *
                    self.all_imgs_wh[:sample_ts, 1]).sum())
            rgb_sample_points = rgb_sample_points.asnumpy().astype(int)
            rays = ms.Tensor(self.all_rays[rgb_sample_points, :8],
                             dtype=ms.float32)
            ts = ms.Tensor(self.all_rays[rgb_sample_points, 8],
                           dtype=ms.int32)
            rgbs = ms.Tensor(self.all_rgbs[rgb_sample_points],
                             dtype=ms.float32)
            whole_img = ms.Tensor(img,
                                  dtype=ms.float32).unsqueeze(0)
            img_wh = ms.Tensor(self.all_imgs_wh[sample_ts],
                               dtype=ms.float32)
            uv_sample = ms.Tensor(uv_sample, dtype=ms.float32)
            self.step += 1
        else:
            if self.split == 'val':
                id_ = self.val_id
            elif self.split == 'test_test':
                id_ = self.img_ids_test[idx]
            c2w = np.array(self.poses_dict[id_])
            img = \
                Image.open(os.path.join(self.root_dir, 'dense/images',
                                        self.image_paths[id_])).convert('RGB')
            img_w, img_h = img.size
            if self.img_downscale > 1:
                img_w = img_w // self.img_downscale
                img_h = img_h // self.img_downscale
            img_s = img.resize((img_w, img_h), Image.LANCZOS)
            img_s = ms.Tensor(self.transform(img_s),
                              dtype=ms.float32)  # (3, h, w)
            rgbs = img_s.view(3, -1).permute(1, 0)  # (h*w, 3) RGB
            rays_o, _, rays_d = get_rays(img_h, img_w, self.Ks[id_], c2w)
            rays_o, rays_d = ms.Tensor(rays_o), ms.Tensor(rays_d)
            rays = ops.cat([rays_o, rays_d,
                            ms.Tensor(self.nears[id_]) *
                            ops.ones_like(rays_o[:, :1]),
                            ms.Tensor(self.fars[id_]) *
                            ops.ones_like(rays_o[:, :1])], 1)  # (h*w, 8)
            ts = id_ * ops.ones(rays.shape[0], dtype=ms.int32)
            img_wh = ms.Tensor([img_w, img_h], dtype=ms.float32)
            w_samples, h_samples = \
                ops.meshgrid(ops.linspace(0, 1 - 1 / img_w, int(img_w)),
                             ops.linspace(0, 1 - 1 / img_h, int(img_h)),
                             indexing='ij')
            uv_sample = ops.cat(
                (h_samples.permute(1, 0).view(-1, 1),
                 w_samples.permute(1, 0).view(-1, 1)), -1)
            img_w, img_h = img.size
            img_8 = img.resize((img_w // self.img_downscale_appearance,
                                img_h // self.img_downscale_appearance),
                               Image.LANCZOS)
            img_8 = ms.Tensor(
                self.normalize(self.transform(img_8)))  # (3, h, w)
            whole_img = img_8.unsqueeze(0)
            rays = rays.astype(ms.float32)
            ts = ts.astype(ms.int32)
            rgbs = rgbs.astype(ms.float32)
            whole_img = whole_img.astype(ms.float32)
            uv_sample = uv_sample.astype(ms.float32)
        return rays, ts, rgbs, whole_img, img_wh, uv_sample


def create_datasets(ds_path,
                    img_downscale: int = 1,
                    use_cache: bool = False,
                    batch_size: int = 1024,
                    scale_anneal: int = -1,
                    min_scale: float = 0.25,
                    train_shuffle: bool = True,
                    num_parallel_workers: int = 1,
                    num_shards: int = 1,
                    shard_id: int = 0):
    train_ds = PhotoTourismDataset(root_dir=ds_path,
                                   split='train',
                                   img_downscale=img_downscale,
                                   val_num=1,
                                   use_cache=use_cache,
                                   batch_size=batch_size,
                                   scale_anneal=scale_anneal,
                                   min_scale=min_scale)
    train_ds = ms.dataset.GeneratorDataset(
        source=train_ds,
        column_names=['rays', 'ts', 'rgbs', 'whole_img',
                      'img_wh', 'uv_sample'],
        column_types=[ms.float32, ms.int32, ms.float32, ms.float32,
                      ms.float32, ms.float32],
        shuffle=train_shuffle,
        num_parallel_workers=num_parallel_workers,
        num_shards=num_shards,
        shard_id=shard_id,
    )

    val_ds = PhotoTourismDataset(root_dir=ds_path,
                                 split='test_test',
                                 img_downscale=img_downscale,
                                 val_num=1,
                                 use_cache=use_cache,
                                 batch_size=batch_size,
                                 scale_anneal=scale_anneal,
                                 min_scale=min_scale)
    val_ds = ms.dataset.GeneratorDataset(
        source=val_ds,
        column_names=['rays', 'ts', 'rgbs', 'whole_img',
                      'img_wh', 'uv_sample'],
        column_types=[ms.float32, ms.int32, ms.float32, ms.float32,
                      ms.float32, ms.float32],
        shuffle=False,
        num_parallel_workers=num_parallel_workers)
    return train_ds, val_ds
