# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/rover-xingyu/Ha-NeRF
# ============================================================================
"""NeRF explicit 3D model scene representation."""

import mindspore.nn as nn
import mindspore.ops as ops
from mindspore import ms_class


class NeRF(nn.Cell):
    def __init__(self, typ,
                 depth=8, width=256, skips=(4,),
                 in_channels_xyz=63, in_channels_dir=27,
                 encode_appearance=False, in_channels_a=48,
                 encode_random=False):

        super().__init__()
        self.typ = typ
        self.depth = depth
        self.skips = skips
        self.in_channels_xyz = in_channels_xyz
        self.in_channels_dir = in_channels_dir
        self.encode_appearance \
            = False if typ == 'coarse' else encode_appearance
        self.in_channels_a = in_channels_a if self.encode_appearance else 0
        self.encode_random = False if typ == 'coarse' else encode_random

        # xyz encoding layers
        xyz_encoding = []
        for i in range(depth):
            if i == 0:
                xyz_encoding.append(nn.Dense(in_channels_xyz,
                                             width, activation='relu'))
            elif i in skips:
                xyz_encoding.append(nn.Dense(width + in_channels_xyz,
                                             width, activation='relu'))
            else:
                xyz_encoding.append(nn.Dense(width,
                                             width, activation='relu'))
        self.xyz_encoding = nn.CellList(xyz_encoding)
        self.xyz_encoding_final = nn.Dense(width, width)

        # static output layers
        self.static_sigma = nn.Dense(width, 1, activation=ops.Softplus())

        self.dir_encoding = \
            nn.Dense(width + in_channels_dir + self.in_channels_a,
                     width // 2, activation='relu')
        self.static_rgb = nn.Dense(width // 2, 3, activation=nn.Sigmoid())
        self.concat = ops.Concat(axis=-1)

    def construct(self, x, sigma_only=False, output_random=True):
        input_dir_a = False
        input_dir_a_random = False
        if sigma_only:
            input_xyz = x
        elif output_random:
            input_xyz, input_dir, input_a, input_random_a = \
                ops.split(x, [self.in_channels_xyz,
                              self.in_channels_dir,
                              self.in_channels_a,
                              self.in_channels_a],
                          axis=-1)
            input_dir_a = ops.cat((input_dir, input_a), -1)
            input_dir_a_random = ops.cat((input_dir, input_random_a), -1)
        else:
            input_xyz, input_dir_a = \
                ops.split(x, [self.in_channels_xyz,
                              self.in_channels_dir + self.in_channels_a],
                          axis=-1)

        for i in range(self.depth):
            if i in self.skips:
                input_xyz = self.concat((x[..., :self.in_channels_xyz],
                                         input_xyz))
            input_xyz = self.xyz_encoding[i](input_xyz)

        static_sigma = self.static_sigma(input_xyz)  # (B, 1)
        if sigma_only:
            return static_sigma

        xyz_encoding_final = self.xyz_encoding_final(input_xyz)
        dir_encoding_input = ops.cat([xyz_encoding_final, input_dir_a], 1)
        dir_encoding = self.dir_encoding(dir_encoding_input)
        static_rgb = self.static_rgb(dir_encoding)  # (B, 3)
        static = ops.cat([static_rgb, static_sigma], 1)  # (B, 4)

        if output_random:
            dir_encoding_input_random = \
                ops.cat([xyz_encoding_final, input_dir_a_random], 1)
            dir_encoding_random = self.dir_encoding(dir_encoding_input_random)
            static_rgb_random = self.static_rgb(dir_encoding_random)  # (B, 3)
            return ops.cat([static, static_rgb_random], 1)  # (B, 7)

        return static


@ms_class
class PosEmbedding:
    def __init__(self, max_logscale, N_freqs, logscale=True):
        """
        Defines a function that embeds x to (x, sin(2^k x), cos(2^k x), ...)
        """
        super().__init__()
        self.concat = ops.Concat(axis=-1)

        if logscale:
            self.freqs = 2 ** ops.linspace(0, max_logscale, N_freqs)
        else:
            self.freqs = ops.linspace(1, 2 ** max_logscale, N_freqs)

    def embed(self, x):
        """
        Inputs:
            x: (B, 3)
        Outputs:
            out: (B, 6*N_freqs+3)
        """
        output = x
        for freq in self.freqs:
            output = self.concat((output, ops.sin(x * freq)))
            output = self.concat((output, ops.cos(x * freq)))

        return output
