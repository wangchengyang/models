# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/kakaobrain/nerf-factory
# ============================================================================
"""Volume rendering pipeline."""

import mindspore as ms

from mindspore import nn
from mindspore import ops

from .coordinates_samplers import DepthCoordsBuilder, HierarchicalCoordsBuilder
from .scene_representation import PosAndDirEncoder, NeRFMLP


class NerfToOutput(nn.Cell):

    def __init__(self, white_bkgr: bool,
                 perturbation: bool = False,
                 density_noise: float = 0.0,
                 density_bias: float = -1,
                 rgb_padding: float = 0.001,
                 dtype=ms.float32):
        super().__init__()
        self.white_bkgr = white_bkgr
        self.perturbation = perturbation
        self.density_noise = density_noise
        self.density_bias = density_bias
        self.rgb_padding = rgb_padding
        self.dtype = dtype

        # Ops.
        self.exp = ops.Exp()
        self.lpnorm = ops.LpNorm(axis=-1)
        self.relu = ms.nn.ReLU()

        self.density_activation = ops.Softplus()
        self.rgb_activation = nn.Sigmoid()
        self.reduce_sum = ops.ReduceSum(keep_dims=False)
        self.cum_sum = ops.CumSum()
        self.concat = ops.Concat(axis=-1)

    def process(self, raw_rgb, raw_density, t_vals, rays_d):

        # Activation.
        if self.perturbation and (self.density_noise > 0):
            raw_density += self.density_noise * ops.randn_like(raw_density, dtype=ms.float32)

        rgb = self.rgb_activation(raw_rgb)
        rgb = rgb * (1 + 2 * self.rgb_padding) - self.rgb_padding
        density = self.density_activation(raw_density + self.density_bias)

        # Volumetric Rendering.
        t_mids = 0.5 * (t_vals[..., :-1] + t_vals[..., 1:])
        t_dists = t_vals[..., 1:] - t_vals[..., :-1]
        delta = t_dists * self.lpnorm(rays_d[..., None, :])
        # Note that we're quietly turning density from [..., 0] to [...].
        density_delta = density[..., 0] * delta

        alpha = 1 - self.exp(-density_delta)
        trans = self.exp(
            -self.concat(
                [
                    ops.zeros_like(density_delta[..., :1]),
                    self.cum_sum(density_delta[..., :-1], -1),
                ]
            )
        )
        weights = alpha * trans

        comp_rgb = self.reduce_sum(weights[..., None] * rgb, -2)
        acc = self.reduce_sum(weights, -1)
        distance = self.reduce_sum(weights * t_mids, -1) / acc
        distance = ops.clamp(distance, t_vals[:, 0], t_vals[:, -1])

        if self.white_bkgr:
            comp_rgb = comp_rgb + (1.0 - acc[..., None])

        return comp_rgb, distance, weights


class VolumeRendering(nn.Cell):

    def __init__(self,
                 num_samples: int,
                 net_depth: int = 8,
                 net_width: int = 256,
                 white_bkgr: bool = False,
                 dtype: ms.Type = ms.float32,
                 lindisp: bool = False,
                 perturbation: bool = False,
                 near: float = 0.0,
                 far: float = 1.0,
                 density_noise: float = 0.0,
                 density_bias: float = -1,
                 rgb_padding: float = 0.001,
                 resample_padding: float = 0.01,
                 stop_level_grad: bool = True,
                 ray_shape: str = 'cone',
                 min_deg_point: int = 0,
                 max_deg_point: int = 16,
                 deg_view: int = 4,
                 append_identity: bool = True
                 ):
        super().__init__()
        self.nerf_output_ch = 5
        self.dtype = dtype
        self.near = near
        self.far = far

        self.depth_coords_builder = DepthCoordsBuilder(
            num_samples, lindisp, perturbation, ray_shape, dtype)
        self.hierarchical_builder = HierarchicalCoordsBuilder(
            num_samples, perturbation, ray_shape,
            stop_level_grad, resample_padding, dtype)
        self.pos_encoder = PosAndDirEncoder(min_deg_point, max_deg_point,
                                            deg_view, append_identity)
        self.nerf_mlp = NeRFMLP(min_deg_point, max_deg_point, deg_view,
                                net_depth, net_width)
        self.nerf2output = NerfToOutput(white_bkgr,
                                        perturbation,
                                        density_noise,
                                        density_bias,
                                        rgb_padding,
                                        dtype=dtype)
        self.concat = ops.Concat(axis=-1)

    def construct(self, rays):
        rays_o = rays[:, :3]
        rays_d = rays[:, 3:6]
        raw_rays_d = rays[:, 6:9]
        radii = rays[:, 9:10]

        # Sample points along rays.
        near = self.near * ops.ones_like(rays_d[..., :1]).astype(self.dtype)
        far = self.far * ops.ones_like(rays_d[..., :1]).astype(self.dtype)
        z_vals, samples = self.depth_coords_builder.build_coords(rays_o, rays_d, radii, near, far)

        # Positional encoding.
        samples_enc = self.pos_encoder.integrated_pos_enc(samples)
        viewdirs_enc = self.pos_encoder.pos_enc(raw_rays_d)

        # Coarse nerf prediction.
        raw_rgb, raw_density = self.nerf_mlp(samples_enc, viewdirs_enc)
        coarse_rgb, _, weights = self.nerf2output.process(raw_rgb, raw_density, z_vals, rays_d)

        # Fine nerf predictions.
        z_vals, samples = self.hierarchical_builder.build_coords(
            rays_o, rays_d, radii, z_vals, weights)
        samples_enc = self.pos_encoder.integrated_pos_enc(samples)
        raw_rgb, raw_density = self.nerf_mlp(samples_enc, viewdirs_enc)
        fine_rgb, distance, weights = self.nerf2output.process(raw_rgb, raw_density, z_vals, rays_d)

        output = self.concat((coarse_rgb, fine_rgb, distance[..., None], weights))
        return output
