# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/kakaobrain/nerf-factory
# ============================================================================

# ------------------------------------------------------------------------------------
# NeRF-Factory
# Copyright (c) 2022 POSTECH, KAIST, Kakao Brain Corp. All Rights Reserved.
# Licensed under the Apache License, Version 2.0 [see LICENSE for details]
# ------------------------------------------------------------------------------------

import numpy as np


def batches_to_images(all_batches, image_sizes):
    all_batches = all_batches.reshape(-1, all_batches.shape[-1])
    ret, curr = [], 0
    for (h, w) in image_sizes:
        ret.append(all_batches[curr: curr + h * w].reshape(h, w, 3))
        curr += h * w
    return ret


def convert_to_ndc(origins, directions, ndc_coeffs, near: float = 1.0):
    """Convert a set of rays to NDC coordinates."""
    t = (near - origins[Ellipsis, 2]) / directions[Ellipsis, 2]
    origins = origins + t[Ellipsis, None] * directions

    dx, dy, dz = directions[:, 0], directions[:, 1], directions[:, 2]
    ox, oy, oz = origins[:, 0], origins[:, 1], origins[:, 2]
    o0 = ndc_coeffs[0] * (ox / oz)
    o1 = ndc_coeffs[1] * (oy / oz)
    o2 = 1 - 2 * near / oz
    d0 = ndc_coeffs[0] * (dx / dz - ox / oz)
    d1 = ndc_coeffs[1] * (dy / dz - oy / oz)
    d2 = 2 * near / oz

    origins = np.stack([o0, o1, o2], -1)
    directions = np.stack([d0, d1, d2], -1)

    return origins, directions


def batchified_get_rays(
        intrinsics,
        extrinsics,
        image_sizes,
        use_pixel_centers,
        get_radii,
        ndc_coord,
        ndc_coeffs,
        multlosses,
):
    radii = None
    multloss_expand = None

    center = 0.5 if use_pixel_centers else 0.0
    mesh_grids = [
        np.meshgrid(
            np.arange(w, dtype=np.float32) + center,
            np.arange(h, dtype=np.float32) + center,
            indexing="xy",
        )
        for (h, w) in image_sizes
    ]

    i_coords = [mesh_grid[0] for mesh_grid in mesh_grids]
    j_coords = [mesh_grid[1] for mesh_grid in mesh_grids]

    dirs = [
        np.stack(
            [
                (i - intrinsic[0][2]) / intrinsic[0][0],
                (j - intrinsic[1][2]) / intrinsic[1][1],
                np.ones_like(i),
            ],
            -1,
        )
        for (intrinsic, i, j) in zip(intrinsics, i_coords, j_coords)
    ]

    rays_o = np.concatenate(
        [
            np.tile(extrinsic[np.newaxis, :3, 3], (1, h * w, 1)).reshape(-1, 3)
            for (extrinsic, (h, w)) in zip(extrinsics, image_sizes)
        ]
    ).astype(np.float32)

    rays_d = np.concatenate(
        [
            np.einsum("hwc, rc -> hwr", dir, extrinsic[:3, :3]).reshape(-1, 3)
            for (dir, extrinsic) in zip(dirs, extrinsics)
        ]
    ).astype(np.float32)

    viewdirs = rays_d
    viewdirs /= np.linalg.norm(viewdirs, axis=-1, keepdims=True)

    if ndc_coord:
        rays_o, rays_d = convert_to_ndc(rays_o, rays_d, ndc_coeffs)

    if get_radii:

        if not ndc_coord:
            rays_d_orig = [
                np.einsum("hwc, rc -> hwr", dir, extrinsic[:3, :3])
                for (dir, extrinsic) in zip(dirs, extrinsics)
            ]
            dx = [
                np.sqrt(np.sum((v[:-1, :, :] - v[1:, :, :]) ** 2, -1))
                for v in rays_d_orig
            ]
            print('dx', len(dx), dx[0].shape)
            dx = [np.concatenate([v, v[-2:-1, :]], 0) for v in dx]
            print('dx', len(dx), dx[0].shape)
            _radii = [v[..., None] * 2 / np.sqrt(12) for v in dx]
            print('_radii', len(_radii), _radii[0].shape)
            radii = np.concatenate([radii_each.reshape(-1) for radii_each in _radii])[
                ..., None
            ]
            print('radii', radii.shape)

        else:
            rays_o_orig, cnt = [], 0
            for (h, w) in image_sizes:
                rays_o_orig.append(rays_o[cnt : cnt + h * w].reshape(h, w, 3))
                cnt += h * w

            dx = [
                np.sqrt(np.sum((v[:-1, :, :] - v[1:, :, :]) ** 2, -1))
                for v in rays_o_orig
            ]
            dx = [np.concatenate([v, v[-2:-1, :]], 0) for v in dx]
            dy = [
                np.sqrt(np.sum((v[:, :-1, :] - v[:, 1:, :]) ** 2, -1))
                for v in rays_o_orig
            ]
            dy = [np.concatenate([v, v[:, -2:-1]], 1) for v in dy]
            _radii = [(vx + vy)[..., None] / np.sqrt(12) for (vx, vy) in zip(dx, dy)]
            radii = np.concatenate([radii_each.reshape(-1) for radii_each in _radii])[
                ..., None
            ]

    if multlosses is not None:
        multloss_expand = np.concatenate(
            [
                np.array([scale] * (h * w))
                for (scale, (h, w)) in zip(multlosses, image_sizes)
            ]
        )[..., None]

    return rays_o, rays_d, viewdirs, radii, multloss_expand


def get_rays(height: int,
             width: int,
             intrinsic: np.ndarray,
             c2w: np.ndarray):
    i, j = np.meshgrid(np.arange(width, dtype=np.float32),
                       np.arange(height, dtype=np.float32),
                       indexing='xy')
    dirs = np.stack([(i - intrinsic[0][2]) / intrinsic[0][0],
                     -(j - intrinsic[1][2]) / intrinsic[1][1],
                     -np.ones_like(i)], -1)
    # Rotate ray directions from camera frame to the world frame.
    rays_d = np.sum(dirs[..., np.newaxis, :] * c2w[:3, :3], -1)
    rays_o = np.broadcast_to(c2w[:3, -1], np.shape(rays_d))
    rays_raw_d = rays_d / np.linalg.norm(rays_d,
                                         axis=-1,
                                         keepdims=True,
                                         ord=2)
    # calculate radii
    dx = np.sqrt(np.sum((rays_d[:-1, :, :] - rays_d[1:, :, :]) ** 2, -1))
    dx = np.concatenate([dx, dx[-2:-1, :]], 0)
    _radii = dx[..., None] * 2 / np.sqrt(12)
    radii = np.reshape(_radii, (-1, 1))

    # reshape
    rays_raw_d = np.reshape(rays_raw_d, (-1, 3))
    rays_o = np.reshape(rays_o, (-1, 3))
    rays_d = np.reshape(rays_d, (-1, 3))
    return rays_o, rays_d, rays_raw_d, radii


def recalculate_rays_to_ndc(height: int,
                            width: int,
                            focal: float,
                            near: float,
                            rays_o: np.ndarray,
                            rays_d: np.ndarray):
    ndc_coeffs = (2 * focal / width, 2 * focal / height)
    t = (near - rays_o[..., 2]) / rays_d[..., 2]
    origins = rays_o + t[..., None] * rays_d

    dx, dy, dz = rays_d[:, 0], rays_d[:, 1], rays_d[:, 2]
    ox, oy, oz = origins[:, 0], origins[:, 1], origins[:, 2]
    o0 = ndc_coeffs[0] * (ox / oz)
    o1 = ndc_coeffs[1] * (oy / oz)
    o2 = 1 - 2 * near / oz
    d0 = ndc_coeffs[0] * (dx / dz - ox / oz)
    d1 = ndc_coeffs[1] * (dy / dz - oy / oz)
    d2 = 2 * near / oz

    rays_o = np.stack([o0, o1, o2], -1)
    rays_d = np.stack([d0, d1, d2], -1)

    # calculate radii
    rays_o_orig = rays_o.reshape((height, width, 3))

    dx = np.sqrt(np.sum((rays_o_orig[:-1, :, :] - rays_o_orig[1:, :, :]) ** 2, -1))
    dx = np.concatenate([dx, dx[-2:-1, :]], 0)
    dy = np.sqrt(np.sum((rays_o_orig[:, :-1, :] - rays_o_orig[:, 1:, :]) ** 2, -1))
    dy = np.concatenate([dy, dy[:, -2:-1]], 1)
    _radii = (dx + dy)[..., None] / np.sqrt(12)
    radii = np.reshape(_radii, (-1, 1))

    return rays_o, rays_d, radii
