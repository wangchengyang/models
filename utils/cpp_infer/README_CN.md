# MindSpore C++推理部署指南

## 简介

本教程针对MindSpore导出的MindIR使用C++进行推理部署的场景。

支持部署在Ascend910、Ascend310、Ascend310P、Nvidia GPU、CPU环境上。

**注意 MindSpore2.0 后 C++ 推理只能使用Lite，使用MindSpore310 进行推理的请使用 MindSpore2.0 之前的版本**

## 安装指南

注意： MindSpore Lite 支持 python3.7。 请在安装只前准备python3.7的环境。

### 安装 MindSpore

请参考MindSpore官网 [MindSpore Install](https://www.mindspore.cn/install) 安装MindSpore。

### 安装 MindSpore Lite

参考MindSpore Lite官网 [Lite install](https://mindspore.cn/lite/docs/zh-CN/master/use/downloads.html)。

1. 根据使用环境下载对应的tar包和whl包

2. 解压 tar.gz 包并且安装相同版本的whl包：

```shell
tar -zxvf mindspore-lite-*.tar.gz
pip install mindspore_lite-*.whl
```

### 设置环境变量

如果在Ascend环境上执行推理，需要设置Ascend的环境变量。`ASCEND_PATH` 是昇腾安装的路径，一般是`/usr/local/Ascend/tookits` 或者 `/usr/local/Ascend/latest`。

```shell
source $ASCEND_PATH/bin/setenv.bash
```

`LITE_HOME` 是 tar.gz 包解压后的路劲，建议使用绝对路径。

```shell
export LITE_HOME=/path/to/mindspore-lite-{version}-{os}-{platform}
export LD_LIBRARY_PATH=$LITE_HOME/runtime/lib:$LITE_HOME/tools/converter/lib:$LD_LIBRARY_PATH
export PATH=$LITE_HOME/tools/converter/converter:$LITE_HOME/tools/benchmark:$PATH
```

**MindSpore 和 MindSpore Lite 必须是相同的版本。**

## 推理流程

一个典型的推理流程包括：

- 导出MindIR文件
- 数据前处理（可选）
- 推理模型编译及执行
- 推理结果后处理

整个过程可以参考[resnet](https://gitee.com/mindspore/models/tree/master/official/cv/ResNet#%E6%8E%A8%E7%90%86%E8%BF%87%E7%A8%8B)（使用C++进行数据处理）和
[DBNet](https://gitee.com/mindspore/models/tree/master/official/cv/DBNet#%E7%A6%BB%E7%BA%BF%E6%8E%A8%E7%90%86)（使用python进行数据处理并转成bin文件）。

### 导出MindIR文件

MindSpore提供了云侧（训练）和端侧（推理）统一的中间表示（Intermediate Representation，[IR](https://www.mindspore.cn/docs/zh-CN/master/design/all_scenarios.html#中间表示mindir)）。可使用export接口直接将模型保存为MindIR。

```python
import mindspore as ms
from src.model_utils.config import config
from src.model_utils.env import init_env
# 环境初始化
init_env(config)
# 推理模型
net = Net()
# 加载参数
ms.load_checkpoint("xxx.ckpt", net)
# 构造输入，只需要构造输入的shape和type就行
inp = ms.ops.ones((1, 3, 224, 224), ms.float32)
# 模型导出，file_format支持'MINDIR', 'ONNX' 和 'AIR'
ms.export(net, ms.export(net, inp, file_name=config.file_name, file_format=config.file_format))
# 当模型是多输入时
# inputs = [inp1, inp2, inp3]
# ms.export(net, ms.export(net, *inputs, file_name=config.file_name, file_format=config.file_format))
```

### 数据前处理(可选)

有一些数据处理在C++侧比较难实现，可以先将数据保存成bin文件。

```python
import os
from src.dataset import create_dataset
from src.model_utils.config import config

dataset = create_dataset(config, is_train=False)
it = dataset.create_dict_iterator(output_numpy=True)
input_dir = config.input_dir
for i,data in enumerate(it):
    input_name = "eval_input_" + str(i+1) + ".bin"
    input_path = os.path.join(input_dir, input_name)
    data['img'].tofile(input_path)
```

将会在`config.input_dir`目录下生成输入的bin文件。

### 推理模型开发

这里涉及C++工程的创建及编译流程。一般一个C++推理工程目录结构如下：

```text
└─cpp_infer
    ├─build.sh                # C++ 编译脚本
    ├─CmakeLists.txt          # Cmake配置文件
    ├─main.cc                 # 模型执行脚本
    └─common_inc              # 公共头文件
```

其中`build.sh`，`CmakeLists.txt`，`common_inc`一般是不用变的，在`example`目录下面提供了一份通用的脚本。

在开发一个新的模型时，需要把这几个文件拷到执行目录下，并编写对应模型的`main.cc`执行。

**有的模型`cpp_infer`下面没有`common_inc`文件夹，在执行的时候需要将这个文件夹拷到`main.cc`同目录下**

`main.cc`里面是做模型推理的脚本文件，一般包括：

- 模型加载及构建
- 数据集构建/bin文件加载
- 模型推理
- 推理结果保存

具体请参考[官网介绍](https://www.mindspore.cn/tutorials/experts/zh-CN/r1.9/infer/ascend_310_mindir.html)，以及已实现的推理模型实现，比如[resnet C++推理](https://gitee.com/mindspore/models/blob/master/official/cv/ResNet/cpp_infer/src/main.cc)。

### 推理模型编译及执行

一般我们会在scripts目录下写一个`run_infer_cpp.sh`的文件将整个推理流程串起来。详情请参考[resnet](https://gitee.com/mindspore/models/blob/master/official/cv/ResNet/scripts/run_infer_cpp.sh)。
